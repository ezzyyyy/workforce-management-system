import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    navContainer: {
        flex: 1,
        paddingHorizontal: 20,
    },
    userContainer: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        flexDirection: 'row',
        padding: 20
    },
    image: {
        width: 70,
        height: 70,
        borderRadius: 35
    },
    detailsContainer: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start'
    },
    userName: {
        fontSize: 15,
        color: '#114880',
        fontWeight: 'bold'
    },
    userEmail: {
        fontSize: 13,
        color: '#999'
    },
    navItemContainer: {
        padding: 5,
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row',
        margin: 10
    },
    navIcon: {
        color: '#114880'
    },
    pageName: {
        color: '#114880',
        paddingHorizontal: 30
    },
    logoutContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        paddingVertical: 20,
        paddingHorizontal: 10
    },
    logoutBtn: {
        padding: 5,
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row',
        margin: 10
    },
    logoutIcon: {
        color: '#114880'
    }
});