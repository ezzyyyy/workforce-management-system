import { StyleSheet } from 'react-native';

export const iosstyle = StyleSheet.create({
    headerIPhoneXorAbove: {
        paddingTop: 25,
        zIndex: 10,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.1,
        backgroundColor: 'rgb(255,255,255)'
    },
    iosPadding: {
        flexDirection: 'column', 
        justifyContent: 'space-between',
        paddingTop: 25
    },
    forgotContainer: {
        paddingTop: 25,
        flex: 1
    },
    switchViewIOS: {
        paddingTop: 25,
        position: 'absolute',
        right: 5,
        flexDirection: 'row', 
        alignItems: 'center',
        justifyContent: 'center'
    },
    reportLocationHeader: {
        zIndex: 10,
        paddingTop: 35,
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10,
        justifyContent: 'flex-start',
        elevation: 30,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.1,
        backgroundColor: 'rgb(255,255,255)'    
    }
})