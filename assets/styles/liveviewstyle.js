import { StyleSheet } from 'react-native';

const LiveViewStyle = StyleSheet.create({
    container: {
        flex: 1
    },
    mapContainer: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0
    },
    actionButton: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(255,255,255,1.0)',
        marginBottom: 10,
        padding: 10,
        width: 48,
        height: 48,
        borderRadius: 3,
        elevation: 3,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.3,
    },
    uploadButton: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'green',
        marginBottom: 10,
        padding: 10,
        width: 48,
        height: 48,
        borderRadius: 3,
        elevation: 3,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.3,
    },
    disabledBtn: {
        backgroundColor: 'rgba(255,255,255,0.2)',
        marginBottom: 10,
        padding: 10,
        borderRadius: 7,
        elevation: 30,
    },
    content: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        flex: 1
    },
    buttonsView: {
        position: 'absolute',
        flexDirection: 'column',
        alignItems: 'flex-start',
        padding: 10,
        zIndex: 10,
        left: 5,
        top: 5,
    },
    intercomView: {
        position: 'absolute',
        padding: 10,
        zIndex: 10,
        right: 5
    },
    vehicleInfo: {
        position: 'absolute',
        alignItems: 'flex-start',
        zIndex: 10,
        bottom: 0
    },
    intercomBtn: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(255,255,255,0)',
        padding: 18,
        borderRadius: 50,
    },
    closeBtn: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'rgba(255,255,255,1.0)',
        alignItems: 'center',
        padding: 18,
        borderRadius: 50,
        elevation: 30,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.1,
    },
    img: {
        width: 30,
        height: 30
    },
    toast: {
        position: 'absolute',
        backgroundColor: '#FFF',
        marginBottom: 30,
        marginHorizontal: 15
    },
    toastText: {
        fontWeight: 'bold',
        color: '#555'
    },
    intercomIcon: {
        color: 'rgba(0,0,0,0)',
        fontSize: 0,
    },
    closeIcon: {
        color: '#114880',
        fontSize: 20,
    },
    sideBtnIcon: {
        color: '#114880',
        fontSize: 26,
    },
    uploadIcon: {
        color: '#FFFFFF'
    },
    shiftBtnContainer: {
        position: 'absolute',
        // width: '100%',
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'row'
        // bottom: 25,
    },
    shiftBtn: {
        paddingVertical: 20,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    shiftBtnText: {
        color: '#FFFFFF',
        fontWeight: 'bold',
        paddingLeft: 10
    },
    loadingView: {
        width: '100%',
        height: '100%',
        zIndex: 3,
        alignSelf: 'center',
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.1,
    },
    spinnerView: {
        position: 'absolute',
        zIndex: 10,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(255,255,255,1)',
        borderRadius: 5,
        elevation: 10,
    },
    spinnerText: {
        color: '#114880',
        fontSize: 13,
        margin: 10
    },
    spinner: {
        width: 75,
        height: 75,
    },
});

export default LiveViewStyle;