import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 40, 
        backgroundColor: '#21A8FF30'
    },
    image: {
        height: '100%',
        width: '100%',
        opacity: 0.3,
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        resizeMode: 'cover'
    },
    logo: {
        flex: 1,
        justifyContent: 'flex-end',
        paddingBottom: 30,
        alignItems: 'center'
    },
    icon: {
        alignSelf: 'center',
        width: 110,
        height: 120
    },
    userInputContainer: {
        height: 60
    },
    label: {
        fontSize: 16,
        color: '#114880'
    },
    loginText: {
        fontWeight: 'bold',
        color: '#FFF'
    },
    pwInputContainer: {
        marginTop: 10,
        height: 60
    },
    buttonContainer: {
        flex: 1,
        justifyContent: 'flex-start'
    },
    button: {
        marginTop: 30,
        paddingVertical: 18,
        backgroundColor: '#114880',
        alignItems: 'center'
    },
    forgotContainer: {
        alignItems: 'center'
    },
    forgotButton: {
        padding: 20
    },
    forgot: {
        color: '#FFFFFF',
    },
    loadingView: {
        width: '100%',
        height: '100%',
        zIndex: 3,
        alignSelf: 'center',
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.1,
    },
    spinnerView: {
        width: 125,
        height: 125,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(255,255,255,1)',
        borderRadius: 5,
        elevation: 10,
    },
    spinnerText: {
        color: '#114880',
        fontSize: 13,
        padding: 10
    },
    spinner: {
        width: 75,
        height: 75,
    },
    searchSection: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    searchIcon: {
        padding: 10,
        paddingHorizontal: 20
    },
    input: {
        flex: 1,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 0,
        backgroundColor: '#fff',
        color: '#114880',
    },
});