import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  list: {
    zIndex: 1,
    padding: 5,
    margin: 0,
    marginBottom: 10,
    backgroundColor: '#FFF',
  },
  cardItem: {
    zIndex: 1,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  reportList: {
    flex: 1
  },
  note: {
    flex: 1,
    margin: 20,
    alignItems: 'center',
    justifyContent: 'center'
  },
  noteText: {
    color: '#999999',
    fontSize: 13
  },
  loadingView: {
    flex: 1,
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 100,
    padding: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.1,
  },
  spinnerView: {
    zIndex: 100,
    justifyContent: 'center',
    alignItems: 'center',
    width: 125,
    height: 125,
    backgroundColor: 'rgba(255,255,255,1)',
    borderRadius: 5,
    elevation: 10,
  },
  spinnerText: {
    color: '#114880',
    fontSize: 13,
    padding: 10
  },
  spinner: {
    width: 75,
    height: 75
  }
})

export const speedingStyles = StyleSheet.create({
  iconGroup: {
    flexDirection: 'column',
    alignItems: 'center',
  },
  icon: {
    color: '#EB4F51',
    fontSize: 45,
  },
  speedText: {
    fontWeight: 'bold',
    color: '#EB4F51'
  },
  detailsBody: {
    marginLeft: 10,
    height: '100%',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  locationBody: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  locationBody2: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginLeft: 20
  },
  label: {
    color: '#999',
    fontSize: 13
  },
  locMarquee: {
    width: '85%',
    fontSize: 13,
    color: '#555'
  },
  timeBody: {
    paddingVertical: 10,
    height: '100%',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  startTimeGroup: {
    marginLeft: 10,
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-start'
  },
  endTimeGroup: {
    marginRight: 10,
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  time: {
    color: '#555',
    fontWeight: 'bold',
    fontSize: 13
  },
  date: {
    color: '#999',
    fontSize: 13
  },
  lineDurationBody: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  line: {
    borderColor: '#555',
    borderBottomWidth: 1.5,
    width: '90%'
  },
  duration: {
    color: '#999',
    fontSize: 13
  }
});

export const messageStyles = StyleSheet.create({
  sectionContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    padding: 5,
    margin: 5,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2'
  },
  icon: {
    padding: 15,
    paddingLeft: 20,
    color: '#ffca2b'
  },
  detailsContainer: {
    flex: 1,
    flexDirection: 'column',
    padding: 10
  },
  body: {
    fontSize: 13,
    color: '#666'
  },
  time: {
    fontSize: 12,
    color: '#999',
    paddingTop: 10,
    textAlign: 'right'
  },
  linkStyle: {
    justifyContent: 'center',
    color: '#2980b9',
    fontSize: 13
  }
});