import { StyleSheet, Platform } from 'react-native';

export const pickerStyles = StyleSheet.create({
    underline: {
        opacity: 0
    },
    inputAndroid: {
        color: '#114880'
    },
    inputIOS: {
        color: '#114880',
        fontSize: 16,
        alignSelf: 'flex-start',
        paddingLeft: 5,
        marginBottom: 20
    },
    inputIOSContainer: {
        alignItems: 'center',
        paddingTop: 15
    },
    icon: {
        height: 20,
        width: 20
    },
    viewContainer: {
        width: '100%'
    }
});

export const datePickerStyles = StyleSheet.create({
    dateInput: {
        borderWidth: 0
    },
    dateText: {
        color: '#555',
        fontSize: 15,
        alignSelf: 'flex-start',
        marginLeft: 5
    }
});

export const reportHeaderStyles = StyleSheet.create({
    headerBody: {
        marginTop: 15,
        backgroundColor: '#FFFFFF',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between',
        zIndex: 10,
    },
    titleBody: {
        width: '100%',
        padding: 10,
        flexDirection: 'row',
        alignItems: 'flex-start',
        borderBottomWidth: 1,
        borderColor: '#D8D8D8',
    },
    title: {
        paddingTop: Platform.OS === 'ios' ? 10 : 0,
        marginVertical: 5,
        color: '#555',
        alignSelf: 'center',
        fontSize: 16,
        fontWeight: 'bold',
        paddingLeft: 10
    },
    pickerGroupBody: {
        paddingVertical: 5,
        flexDirection: 'row',
        alignItems: 'stretch',
        justifyContent: 'space-between',
        borderColor: '#D8D8D8',
        borderBottomWidth: 1,
        width: '100%',
        paddingHorizontal: 10
    },
    pickerContainer: {
        flex: 1,
        flexDirection: 'column'
    },
    label: {
        fontSize: 13,
        color: '#999',
        marginLeft: 5
    },
    vehiclePickerBody: {
        padding: 10,
        width: '100%',
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'center',
        borderColor: '#999'
    },
    datePickerStyle: {
        alignSelf: 'center',
        width: '100%',
    },
    viewReportBtn: {
        width: '100%',
        height: 50,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        borderRadius: 5
    },
    viewReportIcon: {
        color: '#fff',
        fontSize: 20,
        marginRight: 20
    },
    disabled: {
        paddingVertical: 15,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#d3d3d3'
    },
    boxShadowStyle: {
        width: Platform.OS === 'ios' ? 350 : 380,
        height: 50,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        borderRadius: 5
    },
    searchBtnContainer: {
        width: '100%'
    },
    searchBtn: {
        backgroundColor: '#114880',
        paddingVertical: 15,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    searchText: {
        color: '#FFFFFF',
        fontWeight: 'bold'
    }
});
