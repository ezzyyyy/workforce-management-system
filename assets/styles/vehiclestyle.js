import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    scrollView: {
        flex: 1,
        width: '100%',
        backgroundColor: '#FFFFFF'
    },
    content: {
        marginVertical: 5,
        elevation: 2,
        flex: 1,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.05,
    },
    headerContainer: {
        flex: 1 
    },
    headerTopContainer: {
        flex: 1, 
        flexDirection: 'row', 
        borderColor: '#f2f2f2', 
        borderBottomWidth: 1 
    },
    headerBottomContainer: { 
        flex: 1, 
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'center' 
    },
    headerIconView: { 
        justifyContent: 'center' 
    },
    headerIcon: { 
        padding: 20, 
        fontSize: 35 
    },
    headerTopDetailsView: { 
        flex: 1, 
        justifyContent: 'center', 
        padding: 5 
    },
    bottomBtnContainer: { 
        flex: 1, 
        padding: 5, 
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'center' 
    },
    leftBorder: { 
        borderLeftWidth: 1, 
        borderColor: '#f2f2f2' 
    },
    bottomBtnIconView: { 
        justifyContent: 'center' 
    },
    bottomBtnIcon: { 
        padding: 10, 
        fontSize: 20 
    },
    bottomTextTop: { 
        fontSize: 12, 
        color: '#666' 
    },
    bottomTextBottom: { 
        fontSize: 12, 
        color: '#999' 
    },
    vehName: { 
        fontSize: 14, 
        color: '#666' 
    },
    locText: { 
        fontSize: 12, 
        color: '#999' 
    },
    contentAccContainer: { 
        backgroundColor: '#f7f7f7', 
        flexDirection: 'row', 
        padding: 10, 
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center' 
    },
    contentView: { 
        borderRadius: 2, 
        elevation: 2, 
        backgroundColor: '#FFF',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    contentViewDisabled: { 
        borderRadius: 2, 
        backgroundColor: '#FFF',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    disabledIcon: {
        fontSize: 10, 
        color: '#95a5a6', 
        zIndex: 10, 
        position: 'absolute', 
        top: 5, 
        right: 5 
    },
    contentIcon: { 
        padding: 10, 
        fontSize: 20
    },
    installedIcon: {
        paddingLeft: 10, 
        paddingVertical: 10, 
        fontSize: 20
    },
    contentDetailsView: { 
        padding: 10
    },
    active: {
        backgroundColor: '#FFF',
    },
    inactive: {
        backgroundColor: '#FFF',
    },
    rightIconPlus: {
        color: '#27ae60',
        paddingRight: 20
    },
    rightIconMinus: {
        color: 'red',
        paddingRight: 20
    },
    contentContainer: {
        paddingVertical: 7,
    },
    expandPress: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    vehicleList: {
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center'
    },
    vehicleListWrap: {
        backgroundColor: '#FFFFFF', 
        flexDirection: 'row', 
        width: '100%', 
        height: 120,
        zIndex: 10,
    },
    vehicleListHeadersIcon: {
        fontSize: 52,
    },
    vehicleListHeadersText: {
        color: '#FFF', 
        marginTop: 10, 
        fontSize: 18, 
        fontWeight: 'bold', 
        textAlign: 'center'
    },
    button: { 
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center' 
    },
});
