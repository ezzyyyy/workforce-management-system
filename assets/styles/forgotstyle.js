import { StyleSheet, Platform } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    backIcon: {
        paddingTop: Platform.OS === 'ios' ? 10 : 0,
        marginVertical: 5,
        color: '#114880',
        alignSelf: 'center'
    },
    header: {
        width: '100%',
        padding: 10,
        flexDirection: 'column',
        alignItems: 'flex-start',
    },
    forgotTitle: { 
        padding: 10, 
        paddingHorizontal: 20, 
        fontSize: 26, 
        color: '#000',
        fontWeight: 'bold' 
    },
    forgotText: {
        paddingTop: 20,
        paddingHorizontal: 20, 
        color: '#000',
        fontSize: 14 
    },
    resetBtn: {
        flex: 1,
        backgroundColor: '#114880',
        marginTop: 30,
        paddingVertical: 28,
        alignItems: 'center',
        justifyContent: 'center'
    },
    resetText: {
        height: 15,
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    userInput: {
        borderBottomColor: '#11488030'
    },
    emailInput: {
        paddingTop: 10,
        borderBottomColor: '#11488030'
    },
    leftIcon: {
        paddingBottom: 5, 
        color: '#114880',
    },
    inputStyle: {
        fontSize: 15,
        color: '#114880',
        fontWeight: 'bold'
    },
    inputView: {
        padding: 20
    },
    searchSection: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: '#11488070',
        borderBottomWidth: 1
    },
    searchIcon: {
        padding: 10,
        paddingHorizontal: 20
    },
    input: {
        flex: 1,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 0,
        color: '#114880',
    },
});