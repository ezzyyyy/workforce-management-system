import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: { 
        backgroundColor: '#FFF', 
        flex: 1 
      },
      reportList: {
        flex: 1
      },
      note: {
        flex: 1,
        margin: 20,
        alignItems: 'center',
        justifyContent: 'center'
      },
      noteText: {
        color: '#999999',
        fontSize: 13
      },
      loadingView: {
        flex: 1,
        ...StyleSheet.absoluteFillObject,
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 100,
        padding: 10, 
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.1,
      },
      spinnerView: {
        zIndex: 100,
        justifyContent: 'center',
        alignItems: 'center',
        width: 125,
        height: 125,
        backgroundColor: 'rgba(255,255,255,1)',
        borderRadius: 5,
        elevation: 10,
      },
      spinnerText: { 
        color: '#114880', 
        fontSize: 13,
        padding: 10
      },
      spinner: {
        width: 75, 
        height: 75
      }
});