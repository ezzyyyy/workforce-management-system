import { StyleSheet, Platform } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    map: {
        flex: 1,
        top: 0,
        left: 0,
        right: 0,
        bottom: 0
    },
    markerContainer: {
        alignContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    outerBorder: {
        width: 40,
        height: 40,
        borderWidth: 5,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    innerBorder: {
        width: 30,
        height: 30,
        borderColor: '#FFFFFF',
        borderWidth: 3,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center'
    },
    innerText: {
        color: '#FFFFFF',
        fontWeight: 'bold',
        fontSize: 11
    },
    calloutContainer: {
        width: 250,
        height: 50,
        display: 'flex',
        flexDirection: 'column',
    },
    loc: {
        color: '#000000',
        fontWeight: 'bold',
        fontSize: 13
    },
    time: {
        color: '#999999',
        fontSize: 13
    },
    clusterMarker: {
        width: 40,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#2980b9',
        justifyContent: 'center',
        alignItems: 'center'
    },
    outerClusterMarker: {
        width: 60,
        height: 60,
        borderRadius: 30,
        backgroundColor: 'rgba(41,128,185,0.5)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    clusterMarkerText: {
        color: '#FFFFFF',
        fontSize: 15,
        fontWeight: 'bold'
    }
})