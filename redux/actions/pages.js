import {
    UPDATE_VEHICLES, UPDATE_TOKEN, EMPTY_PROPS, LOAD_MAP, LOAD_MARKER, LOAD_GEOFENCES, UPDATE_USER_DATA,
    CHANGE_CONNECTION_STATUS, ADD_TO_ACTION_QUEUE, REMOVE_FROM_ACTION_QUEUE
} from './types'

import { GetVehicles, GetGeofences, SaveLocation } from '../../api/vehicleapi'
import { StartShift, EndShift } from '../../api/eventapi'
import { Login } from '../../api/authenticationapi'
import { GetToken, GetUserData, GetCompanies, GetUserCred } from '../../api/asyncstorage'
import { Alert, AsyncStorage } from 'react-native'
import Intercom from 'react-native-intercom';

let tryCount = 0;

export const SetConnectionState = status => dispatch => {
    dispatch({
        type: CHANGE_CONNECTION_STATUS,
        payload: status
    })
}

export const AddToQueue = pos => async dispatch => {
    dispatch({ type: ADD_TO_ACTION_QUEUE, payload: pos });
}

export const RequestSavePosition = pos => async (dispatch, getState) => {
    let token = await GetToken();
    const { isConnected } = getState();

    if (!!isConnected) {
        SaveLocation(token, pos).then(res => {
            console.log(res)
        })
        pos.map((tp, i) => {
            if (tp.Shift === 'Start') {
                setTimeout(() => {
                    StartShift(token).then(res => {
                        console.log('Connected: Successfully started shift', res)
                    })
                }, 500)
            } else if (tp.Shift === 'End') {
                setTimeout(() => {
                    EndShift(token).then(res => {
                        console.log('Connected: Successfully ended shift', res)
                    })
                }, 1000)
            }
        })
    } else {
        dispatch({ type: ADD_TO_ACTION_QUEUE, payload: pos });
    }
}

export const SaveOfflineTrackpoints = pos => async dispatch => {
    let token = await GetToken();
    SaveLocation(token, pos).then(res => {
        console.log(res)
    })
    pos.map((tp, i) => {
        if (tp.Shift === 'Start') {
            StartShift(token, tp.Timestamp).then(res => {
                console.log('Offline: Successfully started shift', res)
            })
        } else if (tp.Shift === 'End') {
            setTimeout(() => {
                EndShift(token, tp.Timestamp).then(res => {
                    console.log('Offline: Successfully ended shift', res)
                })
            }, 1000)
        }
    })

    dispatch({ type: REMOVE_FROM_ACTION_QUEUE, payload: [] });
    AsyncStorage.removeItem('POS_OFFLINE');
}

export const UpdateUserData = () => dispatch => {
    GetUserData().then((data) => {
        user = JSON.parse(data);
        if (user) {
            GetCompanies().then((data) => {
                let comList = [];
                companies = JSON.parse(data);
                companies.forEach((company) => {
                    let object = {
                        company: company.Name,
                        id: company.CompanyID
                    }
                    comList.push(object);
                })
                dispatch({
                    type: UPDATE_USER_DATA,
                    payload: {
                        user: user,
                        companies: comList
                    }
                })
            })
        }
    })
}

export const UpdateVehicle = () => dispatch => {
    this.isUpdate = true;

    this.setTimeout(() => {
        getVehicles(dispatch);
    }, 1500);

    this.timer = setInterval(() => getVehicles(dispatch), 10000)
}

export const UpdateToken = (data) => dispatch => {
    dispatch({
        type: UPDATE_TOKEN,
        payload: data
    });
}

export const ToggleUpdateVehicle = (status) => dispatch => {
    this.isUpdate = status;
}

export const LoadGeofences = () => dispatch => {
    GetToken()
        .then(token => {
            try {
                GetGeofences(token).then((resp) => {
                    dispatch({
                        type: LOAD_GEOFENCES,
                        payload: resp.GetGeofencesResult
                    })
                }).catch((err) => {
                    console.log(err);
                });
            } catch (err) {
                console.log(err)
            }
        })
}

export const LoadMarker = (marker) => dispatch => {
    dispatch({
        type: LOAD_MARKER,
        payload: marker
    })
}

export const LoadVehicles = () => dispatch => {
    getVehicles(dispatch);
}

export const LoadMap = (map) => dispatch => {
    dispatch({
        type: LOAD_MAP,
        payload: map
    })
}

const getVehicles = async (dispatch) => {
    let token;

    if (this.isUpdate) {
        token = await GetToken();
        userCredStr = await GetUserCred();

        var online = 0;
        var offline = 0;
        var idle = 0;

        GetVehicles(token)
            .then((resp) => {
                resp.GetVehiclesResult.map(v => {
                    if (v.Category == 'Mobile Device') {
                        if (v.LastFix) {
                            if (v.LastFix.Engine == 2) online++;
                            else offline++;
                        }
                        else offline++;
                    }
                });

                dispatch({
                    type: UPDATE_VEHICLES,
                    payload: {
                        vehicles: resp.GetVehiclesResult,
                        online: online,
                        idle: idle,
                        offline: offline
                    }
                })
            })
            .catch((e) => { //handle error
                switch (e.message) {
                    case 'ACCESS_DENIED': // relogin and retry
                        if (tryCount < 3) { //try 2 times
                            // try login
                            GetUserCred().then((cred) => {
                                let userCred = JSON.parse(cred);
                                Login(userCred.u_name, userCred.u_pass).then((result) => {
                                    if (result.LogInResult != undefined) {
                                        AsyncStorage.setItem('USER_TOKEN', result.LogInResult);
                                        tryCount = 0;
                                    } else
                                        tryCount++;
                                }).catch((e2) => { });
                            }).catch((e3) => { });
                            if (tryCount == 2) { //2 failed, try logout
                                AsyncStorage.clear();
                                EmptyProps();
                                Intercom.logout()
                                props.navigation.navigate('Login');
                            }
                        }
                        break;
                }
            });
    }
}

export const EmptyProps = () => dispatch => {
    dispatch({
        type: EMPTY_PROPS,
        payload: {
            geofences: [],
            token: '',
            vehicles: {
                vehicles: [],
                online: 0,
                idle: 0,
                offline: 0
            },
            marker: null,
            title: '',
            userData: null
        }
    })
}