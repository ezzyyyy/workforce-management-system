import { UPDATE_VEHICLES, UPDATE_TOKEN, LOAD_MAP, LOAD_MARKER, LOAD_GEOFENCES, UPDATE_USER_DATA, EMPTY_PROPS, 
    CHANGE_CONNECTION_STATUS, ADD_TO_ACTION_QUEUE, REMOVE_FROM_ACTION_QUEUE } from '../actions/types';
isLoaded = false;
import { AsyncStorage } from 'react-native';

export default function basereducers(state = initiateState, action) {
    switch (action.type) {
        case REMOVE_FROM_ACTION_QUEUE:
            return {
                ...state,
                actionQueue: action.payload
            }
        case ADD_TO_ACTION_QUEUE:
            AsyncStorage.setItem('POS_OFFLINE', JSON.stringify(state.actionQueue.concat(action.payload)))
            return {
                ...state,
                actionQueue: state.actionQueue.concat(action.payload)
            }
        case CHANGE_CONNECTION_STATUS:
            return {
                ...state,
                isConnected: action.payload
            }
        case UPDATE_VEHICLES:
            return {
                ...state,
                vehicles: action.payload
            }
        case UPDATE_TOKEN:
            return {
                ...state,
                token: action.payload
            }
        case LOAD_MAP:
            return {
                ...state,
                map: action.payload
            }
        case LOAD_MARKER:
            return {
                ...state,
                marker: action.payload
            }
        case LOAD_GEOFENCES:
            return {
                ...state,
                geofences: action.payload
            }
        case UPDATE_USER_DATA:
            return {
                ...state,
                userData: action.payload
            }
        case EMPTY_PROPS:
            return {
                ...state,
                geofences: action.payload.geofences,
                marker: action.payload.marker,
                token: action.payload.token,
                vehicles: action.payload.vehicles,
                userData: action.payload.userData
            }
    }
    return state;
}