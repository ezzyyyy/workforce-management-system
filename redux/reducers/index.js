import { combineReducers, applyMiddleware } from 'redux'
import basereducers from './basereducers';

const appReducers = combineReducers({
    basereducers: basereducers,
});

export default appReducers;