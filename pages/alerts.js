import React, { Component } from 'react';
import { View, Text, ActivityIndicator, AsyncStorage, TouchableOpacity } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Icon } from 'react-native-elements';

//components
import AlertsHeader from '../fragments/alertsheader';
import ResultList from '../fragments/resultlist';

//style
import { styles } from '../assets/styles/alertsstyle';

//redux 
import { connect } from 'react-redux'

//API
import { GetMessageReport } from '../api/reportapi';

//asyncstorage
import { GetCompanyID } from '../api/asyncstorage';

const messageReport = {
    type: 'message',
    note: 'To show Alerts report, please select a start time, end time and a vehicle.'
}

var moment = require('moment');
moment().format();

class AlertsPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            headerAnim: 'slideInDown',
            headerStatus: true,
            messageTrackPoints: [],
            trackLength: undefined,
            loading: false,
            report: []
        }
    }


    headerToggle = () => {
        if (!this.state.headerStatus) {
            this.showHeader();
        }
        else {
            this.hideHeader();
        }
    }

    showHeader = () => {
        this.setState({
            headerStatus: true,
            headerAnim: 'slideInDown'
        });
    }

    hideHeader = () => {
        this.setState({
            headerAnim: 'fadeOutUp',
            headerStatus: false
        });
    }

    viewMessageReport = async (token, vehicle, start, end) => {
        this.setState({ messageTrackPoints: [], trackLength: undefined, loading: true });
        let trackPoints = [];
        let count = 300;
        let page = 0;

        let company = await GetCompanyID();

        token = this.state.report.token;
        start = this.state.report.start;
        vehicle = this.state.report.vehicle;
        end = this.state.report.end;

        let lastReportDetails = {token, vehicle, start, end};
        if (lastReportDetails) 
            await AsyncStorage.setItem('LAST_REPORT_DETAILS', JSON.stringify(lastReportDetails));

        try {
            while (count === 300) {
                let result = await GetMessageReport(token, vehicle, start, end, page, company);
                result = result.GetMessageReportResult;
                console.log(result)
                count = result.length;
                if (result.length === 0) {
                    alert('There are no alerts during this period.');
                } else {
                    page = result[result.length - 1].MessageID;
                    result.forEach((point) => {
                        trackPoints.push(point)
                        return true
                    });
                    trackPoints.sort(function (a, b) {
                        return trackPoints[trackPoints.length - 1].MessageID - trackPoints[0].MessageID;
                    });
                    this.setState({ trackLength: trackPoints.length });
                }
            }
            this.setState({ messageTrackPoints: trackPoints, loading: false });
        } catch (error) {
            throw new Error(400);
        }
    }

    getDetails = (data) => { this.setState({ report: data }); }

    render() {
        return (
            <View style={styles.container}>
                {!!this.state.headerStatus &&
                    <Animatable.View
                        useNativeDriver
                        duration={500}
                        animation={this.state.headerAnim}>
                        <AlertsHeader
                            viewReport={this.viewMessageReport}
                            passReportDetails={this.getDetails}
                            hideHeader={this.hideHeader}
                        />
                    </Animatable.View>
                }
                <Animatable.View
                    useNativeDriver
                    duration={500}
                    animation='slideInDown'>
                    <TouchableOpacity onPress={this.headerToggle} style={{ paddingVertical: 10, backgroundColor: '#11488010', height: 40, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                        <Icon name='filter' type='material-community' size={20} iconStyle={{ color: '#114880' }} />
                        <Text style={{ paddingLeft: 5, paddingRight: 20, fontWeight: 'bold', color: '#114880' }}>Filter</Text>
                        <Icon name={!!this.state.headerStatus ? 'chevron-up' : 'chevron-down'} type='material-community' size={20} iconStyle={{ color: '#114880' }} />
                    </TouchableOpacity>
                </Animatable.View>
                <Animatable.View
                    duration={500}
                    useNativeDriver
                    animation={!this.state.headerStatus ? 'slideInUp' : 'slideInDown'}
                    style={styles.reportList}>
                    <View style={styles.reportList}>
                        {(this.state.messageTrackPoints.length !== 0) ? (
                            <ResultList
                                resultType={messageReport.type}
                                dataArray={this.state.messageTrackPoints}
                            />
                        ) : (
                                <View style={styles.note}>
                                    <Text style={styles.noteText}>
                                        {messageReport.note}
                                    </Text>
                                </View>
                            )}
                        {(!!this.state.loading) && (
                            <View style={styles.loadingView}>
                                <View style={styles.spinnerView}>
                                    <ActivityIndicator size='large' style={styles.spinner} color='#114880' />
                                    <Text style={styles.spinnerText}>
                                        {this.state.trackLength ?
                                            ('Loading ' + this.state.trackLength + ' reports...') :
                                            ('Loading...')
                                        }
                                    </Text>
                                </View>
                            </View>
                        )}
                    </View>
                </Animatable.View>
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
    }
}

const mapDispatchToProps = {
    
};

export default connect(mapStateToProps, mapDispatchToProps)(AlertsPage)