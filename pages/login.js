import React from 'react';
import { View, Text, Image, Alert, AsyncStorage, TouchableOpacity, ActivityIndicator, Platform, TextInput } from 'react-native';
import Intercom from 'react-native-intercom';
import * as Animatable from 'react-native-animatable';
import { Icon } from 'react-native-elements';
import { PermissionsAndroid } from 'react-native';

//api
import { Login, GetUser, GetCompanies } from '../api/authenticationapi';

//redux 
import { connect } from 'react-redux'
import { UpdateToken, LoadVehicles, ToggleUpdateVehicle, UpdateUserData, LoadGeofences } from '../redux/actions/pages';

//styles
import { styles } from '../assets/styles/loginstyle';
import Geolocation from '@react-native-community/geolocation';

class LoginPage extends React.Component {
    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);
        this.iconType = 'material-community';
        this.state = {
            username: '',
            password: '',
            token: undefined,
            trackLength: undefined,
            loading: false,
            eye: true
        }
    }

    componentDidMount() {
        if (Platform.OS === 'ios') Geolocation.requestAuthorization()
        else {
            this.requestLocationPermission()
            this.requestCoarseLocationPermission()
        }
    }

    async requestCoarseLocationPermission() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
                {
                    title: 'Location COARSE Permission',
                    message:
                        'WTS needs access to your coarse locations ' +
                        'so you can use our shifts feature.',
                    buttonNeutral: 'Ask Me Later',
                    buttonNegative: 'Cancel',
                    buttonPositive: 'OK',
                },
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log('You can use locations now');
            } else {
                console.log('Location permission denied');
            }
        } catch (err) {
            console.warn(err);
        }
    }

    async requestLocationPermission() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    title: 'Location Permission',
                    message:
                        'WTS needs access to your locations ' +
                        'so you can use our shifts feature.',
                    buttonNeutral: 'Ask Me Later',
                    buttonNegative: 'Cancel',
                    buttonPositive: 'OK',
                },
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log('You can use locations now');
            } else {
                console.log('Location permission denied');
            }
        } catch (err) {
            console.warn(err);
        }
    }

    login = async (username, password) => {
        username = this.state.username;
        password = this.state.password;

        if (!username || !password) {
            Alert.alert(
                'Empty fields!',
                'Please enter your username & password.',
                [{ text: 'OK', onPress: () => { } }]
            );
        } else {
            let result = await Login(username, password);
            let token = result.LogInResult;
            if (!token) {
                Alert.alert(
                    'Incorrect username/password',
                    'Please try again.',
                    [{ text: 'OK', onPress: () => { } }]
                );
            } else {
                this.setState({ loading: true });
                console.log(token)
                let UserCred = { u_name: username, u_pass: password };

                AsyncStorage.setItem('USER_TOKEN', token);
                AsyncStorage.setItem('USER_CRED', JSON.stringify(UserCred));
                this.setState({ trackLength: undefined });

                let result = await GetUser(token);
                let userData = result.GetUserResult;

                console.log(userData)

                Intercom.logEvent('viewed_screen', { extra: 'metadata' });
                Intercom.registerIdentifiedUser({ userId: userData.Name });
                Intercom.updateUser({
                    // Pre-defined user attributes
                    email: userData.Email,
                    user_id: userData.UserID.toString(),
                    name: userData.Name,
                    phone: userData.Phone.toString(),
                    app_id: 'hi30znee',
                    companies: [{
                        company_id: userData.Group.CompanyID.toString(),
                        name: userData.Group.Company
                    }]
                });

                if (!!userData) {
                    AsyncStorage.setItem('USER', JSON.stringify(userData));
                    let companyID = userData.Group.CompanyID;
                    AsyncStorage.setItem('COMPANY_ID', JSON.stringify(companyID));

                    let page = 0;
                    let count = 300;
                    let companies = [];

                    while (count === 300) {
                        let data = await GetCompanies(token, page);
                        let result = data.GetCompaniesResult;
                        count = result.length;

                        //Sort by ascending order
                        result.sort(function (a, b) { return a.CompanyID - b.CompanyID });

                        //Get the highest ID
                        page = result[count - 1].CompanyID;
                        result.forEach((point) => { companies.push(point); return true; });
                        this.setState({ trackLength: companies.length })
                    }
                    AsyncStorage.setItem('USER_COMPANIES', JSON.stringify(companies));
                    this.setState({ loading: false });

                    this.props.UpdateUserData();

                    if (userData.RoleID != 7) {
                        this.props.navigation.navigate('LiveView');
                        this.props.ToggleUpdateVehicle(true);
                        this.props.LoadVehicles();
                        this.props.LoadGeofences(token);
                    }
                    else {
                        this.props.navigation.navigate('Driver');
                    }
                    this.props.UpdateToken(token);
                }
            }
        }
    }

    handlePressIn = () => this.setState({ eye: false })
    handlePressOut = () => this.setState({ eye: true })

    render() {
        return (
            <Animatable.View
                useNativeDriver
                duration={500}
                animation='zoomIn'
                style={{ flex: 1 }}>
                <Image style={styles.image} source={require('../assets/imgs/mbg1.jpg')} />
                <View style={styles.container}>
                    <View style={styles.logo}>
                        <Image style={styles.icon} source={require('../assets/imgs/wts_logo.png')} />
                    </View>
                    <View style={styles.userInputContainer}>
                        <View style={styles.searchSection}>
                            <Icon iconStyle={styles.searchIcon} name='account' size={20} color='#114880' type='material-community' />
                            <TextInput
                                style={styles.input}
                                placeholder='Username'
                                placeholderTextColor='#11488050'
                                selectionColor='#114880'
                                onChangeText={e => this.setState({ username: e })}
                                underlineColorAndroid='transparent'
                            />
                        </View>
                    </View>
                    <View style={styles.pwInputContainer}>
                        <View style={styles.searchSection}>
                            <Icon iconStyle={styles.searchIcon} name='key' size={20} color='#114880' type='material-community' />
                            <TextInput
                                style={styles.input}
                                placeholder='Password'
                                placeholderTextColor='#11488050'
                                selectionColor='#114880'
                                onChangeText={e => this.setState({ password: e })}
                                underlineColorAndroid='transparent'
                                secureTextEntry={this.state.eye}
                            />
                            <TouchableOpacity
                                onPressIn={this.handlePressIn}
                                onPressOut={this.handlePressOut}
                                style={styles.searchIcon}>
                                <Icon name='eye' size={20} color='#114880' type='material-community' />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.buttonContainer}>
                        <TouchableOpacity
                            style={styles.button}
                            onPress={this.state.loading ? () => { } : this.login}>
                            <Text style={styles.loginText}>LOGIN</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.forgotContainer}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('ForgotPassword')}
                            style={styles.forgotButton}>
                            <Text style={styles.forgot}>Forgot Password? Click here</Text>
                        </TouchableOpacity>
                    </View>
                    {(!!this.state.loading) && (
                        <View style={styles.loadingView}>
                            <View style={styles.spinnerView}>
                                <ActivityIndicator size='large' style={styles.spinner} color='#114880' />
                                <Text style={styles.spinnerText}>
                                    {!!this.state.trackLength ?
                                        ('Loading ' + '(' + this.state.trackLength + ')') :
                                        ('Logging in...')}
                                </Text>
                            </View>
                        </View>
                    )}
                </View>
            </Animatable.View>
        );
    }
}

function mapStateToProps(state) {
    return {
        vehicles: state.vehicles,
        map: state.map,
        token: state.token
    }
}

const mapDispatchToProps = {
    ToggleUpdateVehicle,
    LoadVehicles,
    UpdateToken,
    UpdateUserData,
    LoadGeofences
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage)