import React from 'react';
import { TouchableOpacity, View, Dimensions, AsyncStorage, Platform, Alert, Switch, Text, ActivityIndicator, ScrollView } from 'react-native';
import Toast from 'react-native-easy-toast'
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import Intercom from 'react-native-intercom';
import { Icon } from 'react-native-elements';
import Geolocation from '@react-native-community/geolocation';
import NetInfo from '@react-native-community/netinfo';
import Dialog from 'react-native-dialog';
//redux 
import { connect } from 'react-redux'
import { UpdateVehicle, LoadMap, LoadGeofences, SetConnectionState, RequestSavePosition, SaveOfflineTrackpoints, AddToQueue } from '../redux/actions/pages'

//fragments
import LiveMapMarker from '../fragments/markers'
import Geofences from '../fragments/geofences'
import PushController from '../fragments/pushcontroller'

//styles
import LiveViewStyle from '../assets/styles/liveviewstyle'

const mapTypes = ['standard', 'hybrid', 'satellite'];

var moment = require('moment');
moment().format();

class LiveViewPage extends React.Component {
    constructor(props) {
        super(props);
        this.iconType = 'material-community';
        this.mapRef = null;
        this.mapTout = null;
        this.state = {
            regionLat: 1.290270,
            regionLng: 103.851959,
            showTraffic: false,
            activeIndex: 0,
            token: null,
            isLoadingLoc: false,
            checkinStatus: 0,
            showUploadButton: false,
            showConnectionStatus: false,
            isConnected: false,
            dialogVisible: false
        };
        this.isLoaded = false;
        this.zoomedIntoUserMarker = false;
        this.coor;
        this.currentUser = null;
    }

    async componentDidMount() {
        let posOffline = await AsyncStorage.getItem('POS_OFFLINE')
        if (!!posOffline) {
            posOffline = JSON.parse(posOffline);
            console.log(posOffline)
            this.props.AddToQueue(posOffline);
            this.setState({ showUploadButton: true })
        }
        NetInfo.addEventListener(this._handleConnectionChange);

        if (!!this.props.user)
            this.currentUser = this.props.user.user;

        this.props.UpdateVehicle();
        this.props.LoadGeofences();
        this.props.LoadMap(this.mapRef);

        const shift = JSON.parse(await AsyncStorage.getItem('SHIFT'));
        const checkedIn = parseInt(await AsyncStorage.getItem('CHECKED_IN'));

        if (shift !== null) {
            if (shift.status === 1) {
                this.setState({ shiftStatus: 1 })
                Alert.alert(
                    'Shift in progress...',
                    `You did not stop your previous shift. Do you want to continue?`,
                    [{
                        text: 'STOP', onPress: () => {
                            this.endShift();
                        }
                    }, {
                        text: 'CONTINUE', onPress: () => {

                        }
                    }],
                );
                if (checkedIn === 1) this.setState({ checkinStatus: 1 })
            }
        } else {
            //do nothing		
        }
    }

    componentWillUnmount() {
        clearTimeout(this.mapTout);
        NetInfo.removeEventListener(this._handleConnectionChange);
    }

    componentDidUpdate() {
        if (!!this.props.vehicles) {
            const { vehicles } = this.props.vehicles;

            if (!this.isLoaded) {
                var coor = [];
                vehicles.map(v => {
                    if (v.Category == 'Mobile Device') {
                        if (v.LastFix) {
                            coor.push({
                                latitude: v.LastFix.Latitude,
                                longitude: v.LastFix.Longitude
                            });
                        }
                    }
                });

                if (this.mapTout == null && coor.length > 0) {
                    this.mapTout = setTimeout(() => {
                        this.mapRef.fitToCoordinates(coor, {
                            edgePadding: {
                                top: 200,
                                right: 150,
                                bottom: 200,
                                left: 150
                            }
                        });
                        this.isLoaded = true;
                        this.coor = coor;
                    }, 3000);

                }
            }

            if (this.state.shiftStatus === 1 && this.zoomedIntoUserMarker === false) {
                vehicles.map(v => {
                    if (v.LastFix) {
                        if (v.Name === user.Name) {
                            this.mapTout = setTimeout(() => {
                                this.mapRef.animateToRegion({
                                    latitude: v.LastFix.Latitude,
                                    longitude: v.LastFix.Longitude,
                                    latitudeDelta: 0.003,
                                    longitudeDelta: 0.003
                                });
                                this.zoomedIntoUserMarker = true;
                            }, 2000);
                        }
                    }
                })
            }
        }
    }

    renderToastNoLastFix = () => {
        setTimeout(() => {
            if (!!this.props.user) {
                if (this.currentUser !== this.props.user.user) {
                    const { user } = this.props.user;

                    if (!!this.props.vehicles) {
                        const { vehicles } = this.props.vehicles;

                        vehicles.map(v => {
                            if (v.Name === user.Name && user.RoleID !== 7) {
                                if (!v.LastFix) {
                                    setTimeout(() => {
                                        this.refs.toast.show(
                                            'You have no last known location. ' +
                                            'To start tracking your location, ' +
                                            'please press the Start Shift button and wait for at least 60 seconds.');
                                    }, 2000);
                                } else {
                                    //do nothing
                                }
                            }
                        })
                        this.currentUser = user;
                    }
                }
            }
        }, 5000);
    }

    toggleGeofence = (value) => { this.setState({ showGeofences: value }) }
    openIntercom = () => { Intercom.displayMessageComposer() }
    _onPress(vehicle) { this.setState({ vInfo: vehicle }) }

    changeMapStyle = () => {
        let activeIndex = !!this.state.activeIndex ? this.state.activeIndex : 0;
        if (activeIndex === (mapTypes.length - 1)) { activeIndex = 0 }
        else { activeIndex++ }
        this.setState({ activeIndex }, () => {
            this.refs.toast.show('Map type changed to ' +
                mapTypes[this.state.activeIndex] + '!');
        });
    }

    toggleTraffic = () => {
        this.setState({ showTraffic: !this.state.showTraffic });
        this.refs.toast.show(!this.state.showTraffic ? 'Display traffic enabled!' :
            'Display traffic disabled!');
    }

    saveLoc = (shiftStatus) => {
        this.setState({ isLoadingLoc: true })
        Geolocation.getCurrentPosition(async location => {
            let position = [{
                Timestamp: '/Date(' + Date.now() + ')/',
                Latitude: location.coords.latitude,
                Longitude: location.coords.longitude,
            }];
            if (!!shiftStatus) {
                if (shiftStatus === 'start') position[0].Shift = 'Start'
                else if (shiftStatus === 'end') position[0].Shift = 'End'
            }
            this.props.RequestSavePosition(position);

            this.setState({ isLoadingLoc: false })
        }, err => {
            if (err.code === 1)
                Alert.alert(
                    'An error has occured',
                    err.message +
                    ' Please grant location permissions in device settings app and try again.');
        }, {
            enableHighAccuracy: false,
            timeout: 5000,
            maximumAge: 10000
        })
    }

    startShift = async () => {
        Alert.alert(
            'New shift',
            'Tracking will start...',
            [
                {
                    text: 'Cancel',
                    onPress: () => { },
                    style: 'cancel',
                },
                {
                    text: 'Proceed', onPress: async () => {
                        this.saveLoc('start');

                        this.setState({ shiftStatus: 1 })
                        const shift = { status: 1 };
                        await AsyncStorage.setItem('SHIFT', JSON.stringify(shift));
                    }
                }
            ]
        );
    }

    endShift = () => {
        if (this.state.checkinStatus === 1) {
            Alert.alert(
                'Currently checked in...',
                'Please check out before ending your shift.',
                [
                    {
                        text: 'OK',
                        onPress: () => { }
                    }
                ]
            )
        } else {
            Alert.alert(
                'End shift',
                'Tracking will stop...',
                [
                    {
                        text: 'Cancel',
                        onPress: () => { },
                        style: 'cancel',
                    },
                    {
                        text: 'Proceed', onPress: () => {
                            this.saveLoc('end');

                            const shift = { status: 0 };
                            AsyncStorage.setItem('SHIFT', JSON.stringify(shift)).then(async () => {
                                this.setState({ shiftStatus: 0 })
                                await AsyncStorage.removeItem('SHIFT');
                            })

                            if (this.coor !== null || this.coor !== []) {
                                this.mapRef.fitToCoordinates(this.coor, {
                                    edgePadding: {
                                        top: 150,
                                        right: 150,
                                        bottom: 150,
                                        left: 150
                                    }
                                });
                            }
                            this.zoomedIntoUserMarker = false;
                        }
                    }
                ]
            );
        }
    }

    checkIn = () => {
        this.setState({ checkinStatus: 1 }, async () => {
            this.saveLoc();
            const checkedIn = 1;
            await AsyncStorage.setItem('CHECKED_IN', JSON.stringify(checkedIn));
        })
    }

    checkOut = () => {
        this.setState({ checkinStatus: 0 }, async () => {
            this.saveLoc();
            await AsyncStorage.removeItem('CHECKED_IN');
        })
    }

    _handleConnectionChange = ({ isConnected }) => {
        this.props.SetConnectionState(isConnected)

        this.setState({ showConnectionStatus: true, isConnected })
        setTimeout(() => {
            this.setState({ showConnectionStatus: false })
        }, 3000)


        if (!!isConnected && this.props.actionQueue.length > 0) {
            this.setState({ showUploadButton: true })
        }
    }

    uploadOfflineTrackpoints = () => this.setState({ dialogVisible: true })

    handleSave = () => {
        this.props.SaveOfflineTrackpoints(this.props.actionQueue)
        this.setState({ showUploadButton: false, dialogVisible: false })
    }

    handleCancel = () => this.setState({ dialogVisible: false })

    goToAlerts = () => this.props.navigation.navigate('Alerts')

    render() {
        return (
            <View style={LiveViewStyle.container}>
                <Dialog.Container visible={this.state.dialogVisible}>
                    <Text style={{ color: '#3d3f4c', fontSize: 14, fontWeight: 'bold', paddingBottom: 10, marginLeft: 10 }}>Offline tracks</Text>
                    <Text style={{ color: '#abadac', paddingBottom: 10, marginLeft: 10 }}>
                        The items below are shifts/trackpoints that are yet to be saved.
                        To save these trackpoints correctly,
                        please ensure that there is internet connection before pressing the 'Save' button.
                    </Text>
                    <ScrollView>
                        {
                            this.props.actionQueue.map((tp, i) => (
                                <View key={i} style={{ borderBottomColor: '#abadac50', borderBottomWidth: 1, padding: 5, marginHorizontal: 10 }}>
                                    {tp.Shift && tp.Shift === 'Start' && <Text style={{ color: 'green', fontWeight: 'bold' }}>{tp.Shift}</Text>}
                                    <Text style={{ fontSize: 12, color: '#3d3f4c' }}>
                                        {'Timestamp: ' + moment(parseInt(tp.Timestamp.substring(6, 19))).format('DD/MM/YYYY hh:mm:ss A')}
                                    </Text>
                                    <Text style={{ fontSize: 12, color: '#abadac', paddingBottom: 3 }}>
                                        {'Coordinates: ' + tp.Latitude + ', ' + tp.Longitude}
                                    </Text>
                                    {(tp.Shift && tp.Shift === 'End') && <Text style={{ color: 'red', fontWeight: 'bold' }}>{tp.Shift}</Text>}
                                </View>
                            ))
                        }
                    </ScrollView>
                    <Dialog.Button
                        label='Cancel'
                        style={{ color: 'green', fontWeight: 'bold' }}
                        onPress={this.handleCancel} />
                    <Dialog.Button
                        label='Save'
                        style={{ color: !!this.props.isConnected ? 'green' : 'grey', fontWeight: 'bold' }}
                        disabled={!!this.props.isConnected ? false : true}
                        onPress={this.handleSave} />
                </Dialog.Container>
                <PushController goToAlerts={this.goToAlerts} />
                <View style={LiveViewStyle.mapContainer}>
                    <MapView
                        style={LiveViewStyle.map}
                        provider={PROVIDER_GOOGLE}
                        showsUserLocation
                        showsCompass={false}
                        toolbarEnabled={false}
                        mapType={mapTypes[this.state.activeIndex]}
                        showsTraffic={this.state.showTraffic}
                        ref={(ref) => { this.mapRef = ref }}
                        showsCompass={false}
                        toolbarEnabled={false}
                        initialRegion={{
                            latitude: this.state.regionLat,
                            longitude: this.state.regionLng,
                            latitudeDelta: 0.3000,
                            longitudeDelta: 0.3000
                        }}>
                        {(!!this.props.vehicles) ?
                            this.props.vehicles.vehicles.map(v => (
                                (v.LastFix && v.Category == 'Mobile Device') ?
                                    <LiveMapMarker
                                        key={'marker' + v.VehicleID}
                                        onPress={() => this._onPress(v)}
                                        vinfo={{
                                            VehicleID: v.VehicleID,
                                            Category: v.Category,
                                            LastFix: v.LastFix,
                                            Name: v.Name
                                        }} />
                                    : null
                            )) : null}
                        {(!!this.state.showGeofences) ?
                            (<Geofences geofences={this.props.geofences} />) : null}
                    </MapView>
                    {
                        !!this.state.showConnectionStatus &&
                        <View style={{
                            position: 'absolute',
                            backgroundColor: !!this.state.isConnected ? 'green' : 'red',
                            top: 0,
                            left: 0,
                            right: 0,
                            height: 25,
                            zIndex: 11
                        }}>
                            <Text style={{ color: '#FFFFFF', alignSelf: 'center' }}>
                                {!!this.state.isConnected ? 'Connected to Internet!' : 'No internet connection'}
                            </Text>
                        </View>
                    }
                    <View style={{
                        position: 'absolute',
                        right: 15,
                        top: 15,
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: '#FFFFFF',
                        borderRadius: 5
                    }}>
                        <Text style={{ fontSize: 11, color: '#e67e23', fontWeight: 'bold', marginLeft: 10 }}>GEOFENCES</Text>
                        <Switch
                            onValueChange={this.toggleGeofence}
                            value={!!this.state ? this.state.showGeofences : null}
                            style={{ transform: Platform.OS === 'ios' ? [{ scaleX: .5 }, { scaleY: .5 }] : [{ scaleX: .8 }, { scaleY: .8 }] }}
                            thumbColor={'#e67e23'}
                            trackColor={{
                                false: '#e67e2360',
                                true: '#e67e2320'
                            }}
                        />
                    </View>
                    <View style={LiveViewStyle.buttonsView}>
                        <TouchableOpacity
                            onPress={this.changeMapStyle}
                            style={LiveViewStyle.actionButton}>
                            <Icon type={this.iconType} iconStyle={LiveViewStyle.sideBtnIcon} name='layers' />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={this.toggleTraffic}
                            style={LiveViewStyle.actionButton}>
                            <Icon type={this.iconType} iconStyle={LiveViewStyle.sideBtnIcon} name='traffic-light' />
                        </TouchableOpacity>
                        {
                            !!this.state.showUploadButton && (
                                <TouchableOpacity
                                    onPress={this.uploadOfflineTrackpoints}
                                    style={LiveViewStyle.uploadButton}>
                                    <Text style={{ fontSize: 11, color: '#FFFFFF', fontWeight: 'bold' }}>{this.props.actionQueue.length}</Text>
                                    <Icon type={this.iconType} iconStyle={LiveViewStyle.uploadIcon} name='upload' size={20} />
                                </TouchableOpacity>
                            )
                        }
                    </View>
                    <View style={[LiveViewStyle.intercomView, { bottom: this.state.vInfo ? 75 : 0 }]}>
                        <TouchableOpacity
                            onPress={() => { this.state.vInfo ? this.setState({ vInfo: null }) : {} }}
                            style={this.state.vInfo ? LiveViewStyle.closeBtn : {}}>
                            <Icon
                                type={this.iconType}
                                iconStyle={this.state.vInfo ? LiveViewStyle.closeIcon : LiveViewStyle.intercomIcon}
                                name={this.state.vInfo ? 'close' : 'forum'} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: '100%', flexDirection: 'row', position: 'absolute', bottom: 25, paddingHorizontal: 20 }}>
                        <View style={{ width: this.state.shiftStatus === 1 ? '65%' : '100%' }}>
                            <TouchableOpacity
                                onPress={(this.state.shiftStatus === 1) ? this.endShift : this.startShift}
                                style={{
                                    backgroundColor: (this.state.shiftStatus === 1) ? '#21A8FF' : '#1765B3',
                                    width: '100%',
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    paddingVertical: 20
                                }}>
                                <Icon
                                    name={this.state.shiftStatus === 1 ? 'stop' : 'record'}
                                    type='material-community' size={15}
                                    iconStyle={{ color: '#FFFFFF' }} />
                                <Text style={LiveViewStyle.shiftBtnText}>
                                    {this.state.shiftStatus === 1 ?
                                        'END SHIFT' :
                                        'START SHIFT'}
                                </Text>
                            </TouchableOpacity>
                        </View>
                        {
                            this.state.shiftStatus === 1 && (
                                <View style={{ width: '30%' }}>
                                    <TouchableOpacity
                                        onPress={(this.state.checkinStatus === 1) ? this.checkOut : this.checkIn}
                                        style={{
                                            backgroundColor: (this.state.checkinStatus === 1) ? 'orange' : 'green',
                                            width: '100%',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            marginLeft: 15,
                                            paddingVertical: 20
                                        }}>
                                        <Text style={{
                                            color: '#FFFFFF',
                                            fontWeight: 'bold'
                                        }}>{this.state.checkinStatus === 1 ? 'CHECK-OUT' : 'CHECK-IN'}</Text>
                                    </TouchableOpacity>
                                </View>
                            )
                        }
                    </View>
                    {(this.state.vInfo) ?
                        (<View style={LiveViewStyle.vehicleInfo}>
                            <View style={{
                                width: (Dimensions.get('window').width - 20), padding: 10, borderTopRightRadius: 5,
                                borderTopLeftRadius: 5, backgroundColor: 'rgba(255,255,255,1.0)',
                            }}>
                                <Text>User: {this.state.vInfo.Name}</Text>
                                <Text>Address: {this.state.vInfo.LastFix.Location.substring(0, 95)}</Text>
                            </View>
                        </View>) : null}
                    <Toast
                        ref='toast'
                        style={LiveViewStyle.toast}
                        position='bottom'
                        positionValue={200}
                        fadeInDuration={1000}
                        fadeOutDuration={1000}
                        textStyle={LiveViewStyle.toastText}
                    />
                    {this.renderToastNoLastFix()}
                    {
                        !!this.state.isLoadingLoc &&
                        <View style={LiveViewStyle.spinnerView}>
                            <ActivityIndicator size='large' style={LiveViewStyle.spinner} color='#114880' />
                            <Text style={LiveViewStyle.spinnerText}>Please wait...</Text>
                        </View>
                    }
                </View>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        vehicles: state.vehicles,
        marker: state.marker,
        geofences: state.geofences,
        token: state.token,
        user: state.userData,
        isConnected: state.isConnected,
        actionQueue: state.actionQueue
    }
}

const mapDispatchToProps = {
    UpdateVehicle,
    LoadGeofences,
    LoadMap,
    SetConnectionState,
    RequestSavePosition,
    SaveOfflineTrackpoints,
    AddToQueue
};

export default connect(mapStateToProps, mapDispatchToProps)(LiveViewPage)