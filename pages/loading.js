import React from 'react';
import { View, Image } from 'react-native'

//redux 
import { connect } from 'react-redux'

import { UpdateToken, ToggleUpdateVehicle, LoadVehicles, UpdateUserData } from '../redux/actions/pages';

//asyncstorage
import { GetToken, GetUserData } from '../api/asyncstorage';
//styles
import { loadingStyles } from '../assets/styles/loadingstyle';

class LoadingPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: null
        }
    }

    componentDidMount() { this.getToken(); }

    async getToken() {
        let token = await GetToken();
        if (token) {
            this.props.UpdateToken(token);
            this.props.UpdateUserData();

            this.props.ToggleUpdateVehicle(true);
            this.props.LoadVehicles();

            await this.getUser();
            
            if(this.state.user.RoleID == 7)
                setTimeout(() => { this.props.navigation.navigate('Driver') }, 2000);
            else 
            setTimeout(() => { this.props.navigation.navigate('LiveView') }, 2000);
        }
        else this.props.navigation.navigate('Login');
    }

    getUser = async () => {
        let user = await GetUserData();
        this.setState({ user: JSON.parse(user) });
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#21A8FF30' }}>
                <Image style={loadingStyles.image} source={require('../assets/imgs/mbg1.jpg')} />
                    <View style={loadingStyles.container}>
                        <View style={loadingStyles.imageContainer}>
                            <Image style={loadingStyles.icon} source={require('../assets/imgs/wts_logo.png')} />
                        </View>
                    </View>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        vehicles: state.vehicles,
        map: state.map
    }
}

const mapDispatchToProps = {
    UpdateToken,
    UpdateUserData,
    ToggleUpdateVehicle,
    LoadVehicles
};
export default connect(mapStateToProps, mapDispatchToProps)(LoadingPage)
