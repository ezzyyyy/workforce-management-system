import React from 'react';
import { ScrollView, Text, View, TouchableOpacity, Alert, Image } from 'react-native';
import { Icon } from 'react-native-elements';
import Moment from 'react-moment';
import moment from 'moment';
import * as Animatable from 'react-native-animatable';
import Accordion from 'react-native-collapsible/Accordion';

//api
import { ImmobilizeVehicle } from '../api/vehicleapi';

//redux 
import { connect } from 'react-redux'
import { UpdateVehicle, ToggleUpdateVehicle, LoadMarker } from '../redux/actions/pages';

//styles
import { styles } from '../assets/styles/vehiclestyle';

class VehiclesPage extends React.Component {
    constructor(props) {
        super(props);
        this.list = [];
        this.listlength = '...';
        this.iconType = 'material-community';
        this.state = {
            vehicles: [],
            online: 0,
            offline: 0,
            token: undefined,
            statusToggle: 2,
            activeStatus: '#114880',
            activeVehicle: []
        };
    }

    navigateToScreen = (props, route, vehicle) => {
        props.LoadMarker('marker' + vehicle.VehicleID);
        props.map.fitToSuppliedMarkers(['marker' + vehicle.VehicleID]);
        props.navigation.navigate(route);
    }

    componentDidMount() {
        this.props.UpdateVehicle();
    }

    immobilizeVehicle = (vehicle) => {
        Alert.alert(
            ((vehicle.Immobilizer == 1) ? 'Disable' : 'Enable') + ' Immobilizer - ' + vehicle.Name,
            'Please double check the vehicle before proceeding.',
            [{
                text: 'Proceed', onPress: () => {
                    ImmobilizeVehicle(this.props.token, vehicle.TrackerID,
                        ((vehicle.Immobilizer == undefined || vehicle.Immobilizer == 0) ? 1 : 0)).then((resp) => {
                            if (resp.OtaTrackersResult) {
                                Alert.alert(
                                    'Success!',
                                    'The immobilizer has been ' + ((vehicle.Immobilizer == 1) ? 'disabled.' : 'enabled. \n'
                                        + 'This change will be applied shortly.'));
                            }
                            else
                                Alert.alert('Error!', 'Something went wrong, please try again.')
                        }).catch((error) => {
                            Alert.alert('Error!', 'Something went wrong, please try again.')
                        })
                }
            }, { text: 'Cancel', style: 'cancel' }], { cancelable: false })
    }

    passVeh = (veh) => this.setState({ activeVehicle: veh });

    onExpand = (veh) => this.passVeh([this.list.indexOf(veh)]);

    goToMarker = (veh) => {
        if (!veh.LastFix)
            alert('This user has no data currently.');
        else
            this.navigateToScreen(this.props, 'LiveView', veh);
    }

    renderHeader = (veh, _, isActive) => {
        let engColor = '#95a5a6';
        let stat = 'OFFLINE';
        let cutLoc;
        let time;
        let bottomBtns = [];
        // let photo = veh.Photo.includes('https://') ? veh.Photo.replace('https://', 'http://') : veh.Photo;
        let photo = veh.Photo;

        if (veh.LastFix && moment().diff(moment(veh.LastFix.Timestamp), 'days') == 0) {
            if (veh.LastFix.Engine == 1) {
                engColor = '#95a5a6';
                stat = 'OFFLINE'
            }
            else if (veh.LastFix.Engine == 2) {
                engColor = '#27ae60';
                stat = 'ONLINE';
            }
        }
        else {
            engColor = '#95a5a6';
            stat = 'OFFLINE';
        }

        if (veh.LastFix) {
            let loc = veh.LastFix.Location;
            time = veh.LastFix.Timestamp;
            let cut = loc.split(',');
            cutLoc = (cut[0] + ',' + cut[1] + ',' + cut[2] + ', ' + cut[3]);
        }

        let details = [
            {
                containerStyle: styles.bottomBtnContainer,
                icon: 'clock-outline',
                iconStyle: [styles.bottomBtnIcon, { color: engColor }],
                bottomDetails:
                    <View>
                        <Text><Moment style={styles.bottomTextTop} format='h:mm A' element={Text}>{time}</Moment></Text>
                        <Text><Moment style={styles.bottomTextBottom} format='M/D/YY' element={Text}>{time}</Moment></Text>
                    </View>
            },
            {
                containerStyle: [styles.bottomBtnContainer, styles.leftBorder],
                icon: (veh.Category == 'Mobile Device') ? 'cellphone-wireless' : 'cellphone-wireless',
                iconStyle: [styles.bottomBtnIcon, { color: engColor }],
                bottomDetails:
                    <View>
                        <Text style={styles.bottomTextTop}>{stat}</Text>
                        <Text style={styles.bottomTextBottom}>Status</Text>
                    </View>
            }
        ];

        details.map((element, index) => {
            bottomBtns.push(
                <View key={index} style={element.containerStyle}>
                    <View style={styles.bottomBtnIconView}>
                        <Icon
                            name={element.icon}
                            type={this.iconType}
                            iconStyle={element.iconStyle} />
                    </View>
                    {element.bottomDetails}
                </View>
            )
            return bottomBtns;
        })

        return (
            <Animatable.View
                duration={400}
                style={[styles.content, isActive ? styles.active : styles.inactive]}>
                <TouchableOpacity onPress={() => this.goToMarker(veh)} style={styles.headerContainer}>
                    <View style={styles.headerTopContainer}>
                        <View style={styles.headerIconView}>
                            {!!photo ? (
                                <View style={{ padding: 20, width: '100%', height: '100%' }}>
                                    <Image
                                        style={{ borderRadius: 25, width: 50, height: 50 }}
                                        source={{ uri: photo }}
                                    />
                                </View>
                            ) : (
                                    <Icon
                                        name={veh.Category == 'Mobile Device' ? 'human-child' : 'human-child'}
                                        type={this.iconType}
                                        iconStyle={[styles.headerIcon, { color: engColor }]} />
                                )}
                        </View>
                        <View style={styles.headerTopDetailsView}>
                            <Text style={styles.vehName}>{veh.Name}</Text>
                            {(veh.LastFix && (veh.LastFix.Location !== '')) &&
                                <Text style={styles.locText}>{cutLoc}</Text>}
                        </View>
                    </View>
                    <View style={styles.headerBottomContainer}>
                        {bottomBtns}
                    </View>
                </TouchableOpacity>
            </Animatable.View>
        );
    }

    renderContent = (veh, _, isActive) => {
        let immoStat = 'Disabled';

        if (veh.Immobilizer === -1) {
            immoStat = 'Not Installed';
        } else if (veh.Immobilizer === 1) {
            immoStat = 'Enabled';
        }

        return (
            <Animatable.View
                duration={400}
                animation={isActive ? 'zoomIn' : undefined}
                style={[styles.content, isActive ? styles.active : styles.inactive]}>
                <View style={styles.contentAccContainer}>
                    <TouchableOpacity
                        disabled={veh.Immobilizer === -1 ? true : false}
                        onPress={() => this.immobilizeVehicle(veh)}
                        style={veh.Immobilizer === -1 ? styles.contentViewDisabled : styles.contentView}>
                        {((veh.Immobilizer !== -1) && (veh.Category !== 'Mobile Device')) ?
                            (<Icon
                                iconStyle={[styles.installedIcon, { color: (veh.Immobilizer != 1 ? '#95a5a6' : '#27ae60') }]}
                                type={this.iconType}
                                name={(veh.Immobilizer != 1 ? 'lock-open-outline' : 'lock-outline')} />) :
                            (<View>
                                <Icon
                                    iconStyle={styles.disabledIcon}
                                    type={this.iconType}
                                    name='cancel' />
                                <Icon
                                    iconStyle={[styles.contentIcon, { color: '#95a5a6' }]}
                                    type={this.iconType}
                                    name='lock-open-outline' />
                            </View>)}
                        <View style={styles.contentDetailsView}>
                            <Text style={styles.bottomTextTop}>{immoStat}</Text>
                            <Text style={styles.bottomTextBottom}>Immobilizer</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </Animatable.View>
        );
    }

    listVehicles = () => {
        let onlineVeh = [];
        let offlineVeh = [];
        let list = [];
        let all = [];
        let { vehicles } = this.props;
        let { statusToggle } = this.state;

        if (vehicles) {
            vehicles.vehicles.filter(veh => {
                if (veh.Category == 'Mobile Device') {
                    all.push(veh);
                    if (veh.LastFix) {
                        if (veh.LastFix.Engine == 2) {
                            onlineVeh.push(veh);
                            return onlineVeh;
                        } else {
                            offlineVeh.push(veh);
                            return offlineVeh;
                        }
                    } else {
                        offlineVeh.push(veh);
                        return offlineVeh;
                    }
                }
                return all;
            })
            list = all;
            this.listlength = list.length;

            if (statusToggle === 1)
                list = all;
            else if (statusToggle === 2)
                list = onlineVeh;
            else if (statusToggle === 3)
                list = offlineVeh;

            this.list = list;

            return list;
        }
    }

    render() {
        const { activeVehicle, statusToggle, activeStatus } = this.state;
        const { vehicles, navigation } = this.props;

        return (
            <View style={styles.container}>
                <View style={styles.vehicleList}>
                    <View style={styles.vehicleListWrap}>
                        <TouchableOpacity
                            onPress={() => this.setState({ statusToggle: 1 })}
                            style={styles.button}>
                            <Icon
                                type={this.iconType}
                                iconStyle={[styles.vehicleListHeadersIcon,
                                { color: (statusToggle === 1) ? activeStatus : 'rgba(72, 159, 245, 0.2)' }]}
                                name='earth' />
                            <Text
                                style={[styles.vehicleListHeadersText,
                                { color: (statusToggle === 1) ? activeStatus : 'rgba(72, 159, 245, 0.2)' }]}>
                                ALL: {(vehicles) ? this.listlength : 0}
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.setState({ statusToggle: 2 })}
                            style={styles.button}>
                            <Icon
                                type={this.iconType}
                                iconStyle={[styles.vehicleListHeadersIcon,
                                { color: (statusToggle === 2) ? activeStatus : 'rgba(72, 159, 245, 0.2)' }]}
                                name='check-circle-outline' />
                            <Text style={[styles.vehicleListHeadersText,
                            { color: (statusToggle === 2) ? activeStatus : 'rgba(72, 159, 245, 0.2)' }]}>
                                ONLINE: {(vehicles) ? vehicles.online : 0}
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.setState({ statusToggle: 3 })}
                            style={styles.button}>
                            <Icon
                                type={this.iconType}
                                iconStyle={[styles.vehicleListHeadersIcon, {
                                    color: (statusToggle === 3) ? activeStatus : 'rgba(72, 159, 245, 0.2)'
                                }]}
                                name='close-circle-outline' />
                            <Text
                                style={[styles.vehicleListHeadersText, {
                                    color: (statusToggle === 3) ? activeStatus : 'rgba(72, 159, 245, 0.2)'
                                }]}>
                                OFFLINE: {(vehicles) ? vehicles.offline : 0}
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <ScrollView
                        style={styles.scrollView}
                        contentContainerStyle={styles.contentContainer}>
                        {(this.props.vehicles) && (
                            <Accordion
                                activeSections={activeVehicle}
                                sections={this.listVehicles()}
                                touchableComponent={TouchableOpacity}
                                expandMultiple={false}
                                renderHeader={this.renderHeader}
                                renderContent={this.renderContent}
                                duration={300}
                                onChange={() => { }} />
                        )}
                    </ScrollView>
                </View>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        vehicles: state.vehicles,
        map: state.map,
        token: state.token
    }
}

const mapDispatchToProps = {
    ToggleUpdateVehicle,
    UpdateVehicle,
    LoadMarker
};

export default connect(mapStateToProps, mapDispatchToProps)(VehiclesPage)