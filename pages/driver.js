import React from 'react';
import { TouchableOpacity, View, Text, Alert, AsyncStorage, Platform, ActivityIndicator, ScrollView } from 'react-native'
import Intercom from 'react-native-intercom';
import { Icon } from 'react-native-elements';
import * as Animatable from 'react-native-animatable';
import Geolocation from '@react-native-community/geolocation';
import NetInfo from '@react-native-community/netinfo';
import Dialog from 'react-native-dialog';

//redux 
import { connect } from 'react-redux'
import { EmptyProps, UpdateUserData, SetConnectionState, RequestSavePosition, SaveOfflineTrackpoints, AddToQueue } from '../redux/actions/pages';

//styles
import LiveViewStyle from '../assets/styles/liveviewstyle'

//asyncstorage
import { GetToken } from '../api/asyncstorage';

//api
import { Logout } from '../api/authenticationapi';

//fragments
import PushController from '../fragments/pushcontroller'

var moment = require('moment');
moment().format();

class DriverPage extends React.Component {
    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);
        this.iconType = 'material-community';
        this.state = {
            shiftStatus: null,
            isLoadingLoc: false,
            checkinStatus: null,
            showUploadButton: false,
            showConnectionStatus: false,
            isConnected: false,
            dialogVisible: false
        };
        this.isLoaded = false;
    }

    async componentDidMount() {
        let posOffline = await AsyncStorage.getItem('POS_OFFLINE')
        if (!!posOffline) {
            posOffline = JSON.parse(posOffline);
            console.log(posOffline)
            this.props.AddToQueue(posOffline);
            this.setState({ showUploadButton: true })
        }
        NetInfo.addEventListener(this._handleConnectionChange);

        this.props.UpdateUserData();

        const shift = JSON.parse(await AsyncStorage.getItem('SHIFT'));
        const checkedIn = JSON.parse(await AsyncStorage.getItem('CHECKED_IN'));

        if (shift !== null) {
            if (shift.status === 1) {
                this.setState({ shiftStatus: 1 })
                Alert.alert(
                    'Shift in progress...',
                    `You did not stop your previous shift. Do you want to continue?`,
                    [{
                        text: 'STOP', onPress: () => {
                            this.endShift();
                        }
                    }, {
                        text: 'CONTINUE', onPress: () => {

                        }
                    }],
                );
                if (checkedIn === 1) this.setState({ checkinStatus: 1 })
            }
        } else {
            //do nothing		
        }
    }

    componentWillUnmount() {
        NetInfo.removeEventListener(this._handleConnectionChange);
    }

    renderUserData = () => {
        if (this.props.userData) {
            return (
                <Text style={{
                    textAlign: 'center',
                    color: '#1765B3',
                    fontSize: 20,
                    fontWeight: 'bold',
                    marginTop: 10
                }}>Welcome, {this.props.userData.user.Name}!</Text>
            )
        }
    }

    saveLoc = (shiftStatus) => {
        this.setState({ isLoadingLoc: true })
        Geolocation.getCurrentPosition(async location => {
            let position = [{
                Timestamp: '/Date(' + Date.now() + ')/',
                Latitude: location.coords.latitude,
                Longitude: location.coords.longitude,
            }];
            if (!!shiftStatus) {
                if (shiftStatus === 'start') position[0].Shift = 'Start'
                else if (shiftStatus === 'end') position[0].Shift = 'End'
            }
            this.props.RequestSavePosition(position);

            this.setState({ isLoadingLoc: false })
        }, err => {
            if (err.code === 1)
                Alert.alert(
                    'An error has occured',
                    err.message +
                    ' Please grant location permissions in device settings app and try again.');
        }, {
            enableHighAccuracy: false,
            timeout: 5000,
            maximumAge: 10000
        })
    }


    startShift = async () => {
        Alert.alert(
            'New shift',
            'Tracking will start...',
            [
                {
                    text: 'Cancel',
                    onPress: () => { },
                    style: 'cancel',
                },
                {
                    text: 'Proceed', onPress: async () => {
                        this.saveLoc('start');

                        this.setState({ shiftStatus: 1 })
                        const shift = { status: 1 };
                        await AsyncStorage.setItem('SHIFT', JSON.stringify(shift));
                    }
                }
            ]
        );
    }

    endShift = () => {
        if (this.state.checkinStatus === 1) {
            Alert.alert(
                'Currently checked in...',
                'Please check out before ending your shift.',
                [
                    {
                        text: 'OK',
                        onPress: () => { }
                    }
                ]
            )
        } else {
            Alert.alert(
                'End shift',
                'Tracking will stop...',
                [
                    {
                        text: 'Cancel',
                        onPress: () => { },
                        style: 'cancel',
                    },
                    {
                        text: 'Proceed', onPress: () => {
                            this.saveLoc('end');

                            const shift = { status: 0 };
                            AsyncStorage.setItem('SHIFT', JSON.stringify(shift)).then(async () => {
                                this.setState({ shiftStatus: 0 })
                                await AsyncStorage.removeItem('SHIFT');
                            })
                        }
                    }
                ]
            );
        }
    }

    checkIn = () => {
        this.setState({ checkinStatus: 1 }, async () => {
            this.saveLoc();
            const checkedIn = 1;
            await AsyncStorage.setItem('CHECKED_IN', JSON.stringify(checkedIn));
        })
    }

    checkOut = () => {
        this.setState({ checkinStatus: 0 }, async () => {
            this.saveLoc();
            await AsyncStorage.removeItem('CHECKED_IN');
        })
    }

    _handleConnectionChange = ({ isConnected }) => {
        this.props.SetConnectionState(isConnected)

        this.setState({ showConnectionStatus: true, isConnected })
        setTimeout(() => {
            this.setState({ showConnectionStatus: false })
        }, 3000)


        if (!!isConnected && this.props.actionQueue.length > 0) {
            this.setState({ showUploadButton: true })
        }
    }

    uploadOfflineTrackpoints = () => this.setState({ dialogVisible: true })

    handleSave = () => {
        this.props.SaveOfflineTrackpoints(this.props.actionQueue)
        this.setState({ showUploadButton: false, dialogVisible: false })
    }

    handleCancel = () => this.setState({ dialogVisible: false })

    render() {
        return (
            <Animatable.View
                animation='zoomIn'
                useNativeDriver
                duration={500}
                style={LiveViewStyle.container}>
                <PushController goToAlerts={this.goToAlerts} />
                <Dialog.Container visible={this.state.dialogVisible}>
                    <Text style={{ color: '#3d3f4c', fontSize: 14, fontWeight: 'bold', paddingBottom: 10, marginLeft: 10 }}>Offline tracks</Text>
                    <Text style={{ color: '#abadac', paddingBottom: 10, marginLeft: 10 }}>
                        The items below are shifts/trackpoints that are yet to be saved.
                        To save these trackpoints correctly,
                        please ensure that there is internet connection before pressing the 'Save' button.
                    </Text>
                    <ScrollView>
                        {
                            this.props.actionQueue.map((tp, i) => (
                                <View key={i} style={{ borderBottomColor: '#abadac50', borderBottomWidth: 1, padding: 5, marginHorizontal: 10 }}>
                                    {tp.Shift && tp.Shift === 'Start' && <Text style={{ color: 'green', fontWeight: 'bold' }}>{tp.Shift}</Text>}
                                    <Text style={{ fontSize: 12, color: '#3d3f4c' }}>
                                        {'Timestamp: ' + moment(parseInt(tp.Timestamp.substring(6, 19))).format('DD/MM/YYYY hh:mm:ss A')}
                                    </Text>
                                    <Text style={{ fontSize: 12, color: '#abadac', paddingBottom: 3 }}>
                                        {'Coordinates: ' + tp.Latitude + ', ' + tp.Longitude}
                                    </Text>
                                    {(tp.Shift && tp.Shift === 'End') && <Text style={{ color: 'red', fontWeight: 'bold' }}>{tp.Shift}</Text>}
                                </View>
                            ))
                        }
                    </ScrollView>
                    <Dialog.Button
                        label='Cancel'
                        style={{ color: 'green', fontWeight: 'bold' }}
                        onPress={this.handleCancel} />
                    <Dialog.Button
                        label='Save'
                        style={{ color: !!this.props.isConnected ? 'green' : 'grey', fontWeight: 'bold' }}
                        disabled={!!this.props.isConnected ? false : true}
                        onPress={this.handleSave} />
                </Dialog.Container>
                <View style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                    {
                        !!this.state.isLoadingLoc &&
                        <View style={LiveViewStyle.spinnerView}>
                            <ActivityIndicator size='large' style={LiveViewStyle.spinner} color='#114880' />
                            <Text style={LiveViewStyle.spinnerText}>Please wait...</Text>
                        </View>
                    }
                    <TouchableOpacity
                        onPress={async () => {
                            let shiftStatus = await AsyncStorage.getItem('SHIFT');
                            shiftStatus = JSON.parse(shiftStatus);
                            if (shiftStatus !== null) {
                                Alert.alert(
                                    'Shift in progress...',
                                    'You are currently attending to a shift. Please stop your shift before logging out.'
                                );
                            } else {
                                Alert.alert(
                                    'Logout',
                                    'Are you sure?',
                                    [
                                        { text: 'Cancel', onPress: () => { } },
                                        {
                                            text: 'Yes', onPress: async () => {
                                                let token = await GetToken();
                                                if (token) {
                                                    setTimeout(() => {
                                                        this.props.navigation.navigate('Login');
                                                        Intercom.logout();
                                                        AsyncStorage.clear();
                                                        this.props.EmptyProps();
                                                    }, 1000);

                                                    Logout(token).then(res => {
                                                        if (!!res) {
                                                            if (res.LogOutResult.Message == 'OK') {
                                                                console.log('offline');
                                                            } else {
                                                                console.log(res.LogOutResult.Message);
                                                            }
                                                        } else {
                                                            console.log(res)
                                                        }
                                                    })
                                                }
                                            }
                                        }
                                    ])
                            }
                        }
                        }
                        style={{
                            padding: 40,
                            position: 'absolute',
                            bottom: 0,
                            right: 0
                        }}>
                        <Icon
                            type={this.iconType}
                            iconStyle={{
                                color: '#1765B3',
                                fontSize: 30
                            }}
                            name='logout' />
                    </TouchableOpacity>
                    <Animatable.View
                        animation={this.state.shiftStatus == 1 ? 'pulse' : null}
                        easing='ease-out'
                        useNativeDriver
                        iterationCount='infinite'
                        style={{
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}>
                        <Icon
                            type={this.iconType}
                            iconStyle={{
                                color: '#1765B3',
                                fontSize: 80
                            }}
                            name='radar' />
                    </Animatable.View>
                    {this.renderUserData()}
                    <View style={{ marginTop: 20, width: '90%', flexDirection: 'row' }}>
                        <View style={{ width: this.state.shiftStatus === 1 ? '65%' : '100%' }}>
                            <TouchableOpacity
                                onPress={(this.state.shiftStatus === 1) ? this.endShift : this.startShift}
                                style={{
                                    backgroundColor: (this.state.shiftStatus === 1) ? '#21A8FF' : '#1765B3',
                                    width: '100%',
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    paddingVertical: 20
                                }}>
                                <Icon
                                    name={this.state.shiftStatus === 1 ? 'stop' : 'record'}
                                    type='material-community' size={15}
                                    iconStyle={{ color: '#FFFFFF' }} />
                                <Text style={LiveViewStyle.shiftBtnText}>
                                    {this.state.shiftStatus === 1 ?
                                        'END SHIFT' :
                                        'START SHIFT'}
                                </Text>
                            </TouchableOpacity>
                        </View>
                        {
                            this.state.shiftStatus === 1 && (
                                <View style={{ width: '30%' }}>
                                    <TouchableOpacity
                                        onPress={(this.state.checkinStatus === 1) ? this.checkOut : this.checkIn}
                                        style={{
                                            backgroundColor: (this.state.checkinStatus === 1) ? 'orange' : 'green',
                                            width: '100%',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            marginLeft: 15,
                                            paddingVertical: 20
                                        }}>
                                        <Text style={{
                                            color: '#FFFFFF',
                                            fontWeight: 'bold'
                                        }}>{this.state.checkinStatus === 1 ? 'CHECK-OUT' : 'CHECK-IN'}</Text>
                                    </TouchableOpacity>
                                </View>
                            )
                        }
                    </View>
                    {
                        !!this.state.showUploadButton && (
                            <TouchableOpacity
                                onPress={this.uploadOfflineTrackpoints}
                                style={{
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    backgroundColor: 'green',
                                    marginTop: 30,
                                    padding: 10,
                                    width: 48,
                                    height: 48,
                                    borderRadius: 3,
                                    elevation: 3,
                                    shadowColor: '#000',
                                    shadowOffset: {
                                        width: 0,
                                        height: 0,
                                    },
                                    shadowOpacity: 0.3,
                                }}>
                                <Text style={{ fontSize: 11, color: '#FFFFFF', fontWeight: 'bold' }}>{this.props.actionQueue.length}</Text>
                                <Icon type={this.iconType} iconStyle={LiveViewStyle.uploadIcon} name='upload' size={20} />
                            </TouchableOpacity>
                        )
                    }
                </View>
            </Animatable.View>
        );
    }
}

function mapStateToProps(state) {
    return {
        token: state.token,
        userData: state.userData,
        isConnected: state.isConnected,
        actionQueue: state.actionQueue
    }
}

const mapDispatchToProps = {
    EmptyProps,
    UpdateUserData,
    SetConnectionState,
    RequestSavePosition,
    SaveOfflineTrackpoints,
    AddToQueue
};

export default connect(mapStateToProps, mapDispatchToProps)(DriverPage)