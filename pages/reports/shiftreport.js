import React from 'react';
import { View, Text, ActivityIndicator, AsyncStorage, TouchableOpacity } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Icon } from 'react-native-elements'

//components
import ReportHeader from '../../fragments/reportheader';
import ResultList from '../../fragments/resultlist';

//API
import { GetShiftReport, GetPositionReport } from '../../api/reportapi';

//asyncstorage
import { GetCompanyID } from '../../api/asyncstorage'

//styles
import { styles } from '../../assets/styles/reportsstyle';

//redux 
import { connect } from 'react-redux'
import { UpdateVehicle } from '../../redux/actions/pages';

const shiftReport = {
    type: 'shift',
    note: 'To show Shift report, please select a start time, end time and a user.'
}

var moment = require('moment');
moment().format();

class ShiftReportPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            headerAnim: 'slideInDown',
            headerStatus: true,
            shiftTrackPoints: [],
            checkPoints: [],
            trackLength: undefined,
            loading: false,
            report: []
        };
    }

    componentDidMount() {
        this.props.UpdateVehicle();
    }

    headerToggle = () => {
        if (!this.state.headerStatus) {
            this.showHeader();
        }
        else {
            this.hideHeader();
        }
    }

    showHeader = () => {
        this.setState({
            headerStatus: true,
            headerAnim: 'slideInDown'
        });
    }

    hideHeader = () => {
        this.setState({
            headerAnim: 'fadeOutUp',
            headerStatus: false
        });
    }

    viewCheckpoints = async (token, vehicle, start, end) => {
        this.setState({ checkPoints: [], loading: true });
        let positions = [];
        let count = 300;
        let page = 0;

        let company = await GetCompanyID();

        token = this.state.report.token;
        vehicle = this.state.report.vehicleId;
        start = this.state.report.start;
        end = this.state.report.end;

        console.log(this.state.report)

        try {
            while (count === 300) {
                let result = await GetPositionReport(token, start, end, vehicle, page);
                result = result.GetPositionReportResult;
                count = result.length;

                if (count === 0) {
                    alert('No trackpoints recorded for this shift.');
                } else {
                    Array.prototype.push.apply(positions, result);
                    if (count > 0)
                        posID = result[count - 1].PosID;
                }
            }
            this.setState({ checkPoints: positions, loading: false })
        } catch (error) {
            alert(error)
        }
    }

    viewShiftReport = async (token, user, start, end) => {
        this.setState({ shiftTrackPoints: [], trackLength: undefined, loading: true });
        let trackPoints = [];
        let count = 300;
        let page = 0;

        let company = await GetCompanyID();

        token = this.state.report.token;
        user = this.state.report.user;
        start = this.state.report.start;
        end = this.state.report.end;

        let lastReportDetails = { token, user, start, end };
        if (lastReportDetails) {
            await AsyncStorage.setItem('LAST_REPORT_DETAILS', JSON.stringify(lastReportDetails));
        }

        try {
            while (count === 300) {
                let result = await GetShiftReport(token, start, end, user, page);
                result = result.GetShiftReportResult;
                count = result.length;

                if (count === 0) {
                    alert('There are no reports for this user during this period. Please try again.');
                } else {
                    page = result[result.length - 1].ShiftID;
                    result.forEach((point) => {
                        trackPoints.push(point)
                        return true
                    });
                    trackPoints.sort(function (a, b) {
                        return trackPoints[trackPoints.length - 1].ShiftID - trackPoints[0].ShiftID;
                    });
                    this.setState({ trackLength: trackPoints.length });
                }
            }
            this.setState({ shiftTrackPoints: trackPoints, loading: false });
        } catch (error) {
            throw new Error(400);
        }
    }

    getDetails = (data) => {
        let vehicleId = undefined;

        this.props.vehicles.vehicles.map((vehicle, index) => {
            if (vehicle.DriverID == data.user) {
                vehicleId = vehicle.VehicleID;
                return vehicleId;
            }
        })

        console.log(data)

        this.setState({
            report: {
                token: data.token,
                vehicleId: vehicleId,
                user: data.user,
                start: data.start,
                end: data.end
            }
        });
    }

    goToMap = (data) => { this.props.navigation.navigate('ReportLocation', data) }

    render() {
        return (
            <View style={styles.container}>
                {!!this.state.headerStatus &&
                    <Animatable.View
                        useNativeDriver
                        duration={500}
                        animation={this.state.headerAnim}>
                        <ReportHeader
                            viewReport={this.viewShiftReport}
                            viewCheckpoints={this.viewCheckpoints}
                            passReportDetails={this.getDetails}
                            hideHeader={this.hideHeader}
                        />
                    </Animatable.View>}
                <Animatable.View
                    useNativeDriver
                    duration={500}
                    animation='slideInDown'>
                    <TouchableOpacity
                        onPress={this.headerToggle}
                        style={{
                            paddingVertical: 10,
                            backgroundColor: '#11488010',
                            height: 40,
                            alignItems: 'center',
                            justifyContent: 'center',
                            flexDirection: 'row'
                        }}>
                        <Icon name='filter' type='material-community' size={20} iconStyle={{ color: '#114880' }} />
                        <Text style={{ paddingLeft: 5, paddingRight: 20, fontWeight: 'bold', color: '#114880' }}>Filter</Text>
                        <Icon name={!!this.state.headerStatus ? 'chevron-up' : 'chevron-down'} type='material-community' size={20} iconStyle={{ color: '#114880' }} />
                    </TouchableOpacity>
                </Animatable.View>
                <Animatable.View
                    duration={500}
                    useNativeDriver
                    animation={!this.state.headerStatus ? 'slideInUp' : 'slideInDown'}
                    style={styles.reportList}>
                    {(this.state.shiftTrackPoints.length !== 0) ? (
                        <ResultList
                            resultType={shiftReport.type}
                            dataArray={this.state.shiftTrackPoints}
                            checkPointsArray={this.state.checkPoints}
                            goToShiftMap={this.goToMap}
                        />
                    ) : (
                            <View style={styles.note}>
                                <Text style={styles.noteText}>
                                    {shiftReport.note}
                                </Text>
                            </View>
                        )}
                    {(!!this.state.loading) && (
                        <View style={styles.loadingView}>
                            <View style={styles.spinnerView}>
                                <ActivityIndicator size='large' style={styles.spinner} color='#114880' />
                                <Text style={styles.spinnerText}>
                                    {this.state.trackLength ?
                                        ('Loading ' + this.state.trackLength + ' reports...') :
                                        ('Loading...')
                                    }
                                </Text>
                            </View>
                        </View>
                    )}
                </Animatable.View>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        vehicles: state.vehicles,
        token: state.token
    }
}

const mapDispatchToProps = {
    UpdateVehicle,
};

export default connect(mapStateToProps, mapDispatchToProps)(ShiftReportPage)