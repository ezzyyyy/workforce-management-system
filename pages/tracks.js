import React from 'react';
import { Alert, Text, FlatList, View, TouchableOpacity, ActivityIndicator, AsyncStorage } from 'react-native';
import { Icon, Card } from 'react-native-elements';
import * as Animatable from 'react-native-animatable';

var moment = require('moment');
moment().format();

//redux 
import { connect } from 'react-redux'

//components
import TrackHeader from '../fragments/trackheader';
import MapTracks from '../fragments/maptracks'

//API
import { GetPositionReport } from '../api/reportapi';

//styles
import { styles, speedingStyles } from '../assets/styles/resultliststyle'

const trackpointsReport = {
    type: 'trackpoints',
    note: 'To show Trackpoints report, please select a start time, end time and a user.'
}

class TracksPage extends React.Component {
    _keyExtractor = (item, index) => item.PosID.toString();
    constructor(props) {
        super(props);
        this.iconType = 'material-community';
        this.state = {
            headerAnim: 'slideInDown',
            headerStatus: true,
            latLng: [],
            draw: false,
            trackPoints: [],
            showLoader: false,
            loaderText: '',
        };
    }

    getTracks = async (state) => {
        if (state.vehicle == 0)
            Alert.alert('Please select a user.');
        else if (moment(new Date(state.startDate)) > moment(new Date(state.endDate)))
            Alert.alert('End date should be later than start date.');
        else {
            try {
                let count = 300;
                let posID = 0;
                positions = [];
                start = '/Date(' + moment(new Date(state.startDate)) + ')/';
                end = '/Date(' + moment(new Date(state.endDate)) + ')/';

                let lastReportDetails = { vehicle: state.vehicle, start, end };
                if (lastReportDetails)
                    await AsyncStorage.setItem('LAST_REPORT_DETAILS', JSON.stringify(lastReportDetails));


                this.setState({ trackPoints: [], draw: false, showLoader: true, loaderText: 'Loading...' });
                while (count == 300) {
                    await GetPositionReport(this.props.token, start, end, state.vehicle, posID)
                        .then((res) => {
                            count = res.GetPositionReportResult.length;
                            if (count === 0) {
                                alert('There are no reports for this user during this period. Please try again.');
                            } else {
                                Array.prototype.push.apply(positions, res.GetPositionReportResult);
                                this.setState({ loaderText: 'Loading (' + positions.length + ')' });
                                if (count > 0)
                                    posID = res.GetPositionReportResult[count - 1].PosID;
                            }
                        })
                        .catch((error) => {
                            Alert.alert(error);
                        });
                }
                this.setState({ trackPoints: positions, showLoader: false });
            } catch {
                Alert.alert('Error', 'Oops, something went wrong, please try again.');
            }
        }
    }

    drawTracks = () => {
        var latLng = [];
        this.state.trackPoints.map(v => {
            latLng.push({ latitude: v.Latitude, longitude: v.Longitude })
        });
        this.setState({ draw: true, latLng: latLng });
    }

    headerToggle = () => {
        if (!this.state.headerStatus) {
            this.showHeader();
        }
        else {
            this.hideHeader();
        }
    }

    showHeader = () => {
        this.setState({
            headerStatus: true,
            headerAnim: 'slideInDown'
        });
    }

    hideHeader = () => {
        this.setState({
            headerAnim: 'fadeOutUp',
            headerStatus: false
        });
    }

    renderList = ({ item }) => {
        let v = item
        var ignition = 'STOPPED';
        if (v.Engine == 1)
            ignition = 'IDLING';
        else if (v.Engine == 2)
            ignition = 'MOVING';

        let loc = item.Location;
        let cut = loc.split(',');
        let cutLoc = (cut[0] + ',' + cut[1]);

        return (
            <Card id={'track-' + v.PosID}>
                <View style={styles.cardItem}>
                    <View style={speedingStyles.detailsBody}>
                        <View style={speedingStyles.timeBody}>
                            <View style={speedingStyles.startTimeGroup}>
                                <Text style={speedingStyles.time}>{moment(v.Timestamp).format('LTS')}</Text>
                                <Text style={speedingStyles.date}>{moment(v.Timestamp).format('l')}</Text>
                            </View>
                        </View>
                        <View style={speedingStyles.locationBody2}>
                            <Text style={speedingStyles.label}>Location: </Text>
                            <Text style={speedingStyles.locMarquee}>{cutLoc}</Text>
                        </View>
                    </View>
                </View>
            </Card>
        );
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
                {!!this.state.headerStatus &&
                    <Animatable.View
                        useNativeDriver
                        duration={500}
                        animation={this.state.headerAnim}>
                        <TrackHeader
                            token={this.props.token}
                            getTracks={this.getTracks}
                            hideHeader={this.hideHeader}
                        />
                    </Animatable.View>}
                <Animatable.View
                    useNativeDriver
                    duration={500}
                    animation='slideInDown'>
                    <TouchableOpacity onPress={this.headerToggle} style={{ paddingVertical: 10, backgroundColor: '#11488010', height: 40, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                        <Icon name='filter' type='material-community' size={20} iconStyle={{ color: '#114880' }} />
                        <Text style={{ paddingLeft: 5, paddingRight: 20, fontWeight: 'bold', color: '#114880' }}>Filter</Text>
                        <Icon name={!!this.state.headerStatus ? 'chevron-up' : 'chevron-down'} type='material-community' size={20} iconStyle={{ color: '#114880' }} />
                    </TouchableOpacity>
                </Animatable.View>
                {(!this.state.draw) ?
                    (<Animatable.View
                        duration={500}
                        useNativeDriver
                        animation={!this.state.headerStatus ? 'slideInUp' : 'slideInDown'}
                        style={styles.reportList}>
                        {(this.state.trackPoints.length > 0) ?
                            // (<View>
                            //     <FlatList
                            //         contentContainerStyle={{ paddingBottom: 10 }}
                            //         style={styles.list}
                            //         data={this.state.trackPoints}
                            //         keyExtractor={this._keyExtractor}
                            //         renderItem={this.renderList} />
                            //     <View style={{ paddingTop: 10, zIndex: 10, position: 'absolute', top: 0, right: 10 }}>
                            //         <TouchableOpacity
                            //             style={{ width: 50, height: 50, justifyContent: 'center', backgroundColor: '#3D3D3D' }}
                            //             onPress={this.drawTracks}>
                            //             <Icon iconStyle={{ color: '#fff', fontSize: 32 }} type={this.iconType} name='marker' />
                            //         </TouchableOpacity>
                            //     </View>
                            // </View>) 
                            this.drawTracks()
                            :
                            (
                                <View style={styles.note}>
                                    <Text style={styles.noteText}>
                                        {trackpointsReport.note}
                                    </Text>
                                </View>
                            )}
                    </Animatable.View>) :
                    (<MapTracks latLng={this.state.latLng} />)}
                {(this.state.showLoader) ? (
                    <View style={styles.loadingView}>
                        <View style={styles.spinnerView}>
                            <ActivityIndicator size='large' style={styles.spinner} color='#114880' />
                            <Text style={styles.spinnerText}>
                                {this.state.trackLength ?
                                    ('Loading ' + '(' + this.state.trackLength + ')') :
                                    ('Loading...')
                                }
                            </Text>
                        </View>
                    </View>) : null}
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        token: state.token
    }
}

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(TracksPage)