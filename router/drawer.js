import { createDrawerNavigator } from 'react-navigation'
import MainDrawerNavigation from '../fragments/navdrawer'
import { Dimensions } from 'react-native'
const { width, height } = Dimensions.get('screen')

import {
    LiveViewStack,
    PeopleStack,
    ShiftsStack,
    AlertsStack
} from '../router/stack'

export const DrawerStack = createDrawerNavigator({
    LiveView: { screen: LiveViewStack },
    People: { screen: PeopleStack },
    ShiftReport: { screen: ShiftsStack },
    Alerts: { screen: AlertsStack },
}, {
    initialRouteName: 'LiveView',
    contentComponent: MainDrawerNavigation,
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle',
    drawerWidth: Math.min(height, width) * 0.8,
})

