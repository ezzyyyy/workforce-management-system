import { createStackNavigator } from 'react-navigation'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import React from 'react'

import LoginPage from '../pages/login'
import ForgotPasswordPage from '../pages/forgotpassword'

const styles = {
    tintColor: '#114880',
    bgColor: {
        backgroundColor: '#FFFFFF',
    },
    titleStyle: {
        fontSize: 15
    }
}

export const AuthStack = createStackNavigator({
    Login: LoginPage,
    ForgotPassword: {
        screen: ForgotPasswordPage,
        navigationOptions: ({ navigation }) => ({
            title: 'Forgot Password?',
            headerStyle: styles.bgColor,
            headerTintColor: styles.tintColor,
            headerTitleStyle: styles.titleStyle,
        })
    }
})