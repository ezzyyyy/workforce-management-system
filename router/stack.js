
import { createStackNavigator } from 'react-navigation-stack'
import { DrawerActions } from 'react-navigation'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import React from 'react'

import DriverPage from '../pages/driver'
import LiveViewPage from '../pages/liveview'
import VehiclesPage from '../pages/vehicles'
import TracksPage from '../pages/tracks'
import ShiftReportPage from '../pages/reports/shiftreport'
import ReportLocation from '../fragments/reportlocation'
import AlertsPage from '../pages/alerts'

const openDrawer = (navigation) => {
    return (
        <TouchableOpacity onPress={() => navigation.dispatch(DrawerActions.openDrawer())}>
            <Icon name='menu' size={25} color='#114880' style={{ marginLeft: 15 }} />
        </TouchableOpacity>
    )
}

const styles = {
    tintColor: '#114880',
    bgColor: {
        backgroundColor: '#FFFFFF',
    },
    titleStyle: {
        fontSize: 15
    }
}

export const DriverStack = createStackNavigator({
    Driver: {
        screen: DriverPage
    }
});

export const LiveViewStack = createStackNavigator({
    LiveView: {
        screen: LiveViewPage,
        navigationOptions: ({ navigation }) => ({
            title: 'Live View',
            headerLeft: openDrawer(navigation),
            headerStyle: styles.bgColor,
            headerTintColor: styles.tintColor,
            headerTitleStyle: styles.titleStyle,
        })
    }
});

export const PeopleStack = createStackNavigator({
    People: {
        screen: VehiclesPage,
        navigationOptions: ({ navigation }) => ({
            title: 'People',
            headerLeft: openDrawer(navigation),
            headerStyle: styles.bgColor,
            headerTintColor: styles.tintColor,
            headerTitleStyle: styles.titleStyle,
        })
    }
});

export const TracksStack = createStackNavigator({
    Tracks: {
        screen: TracksPage,
        navigationOptions: ({ navigation }) => ({
            title: 'Tracks',
            headerLeft: openDrawer(navigation),
            headerStyle: styles.bgColor,
            headerTintColor: styles.tintColor,
            headerTitleStyle: styles.titleStyle,
        })
    }
});

export const ShiftsStack = createStackNavigator({
    ShiftReport: {
        screen: ShiftReportPage,
        navigationOptions: ({ navigation }) => ({
            title: 'Shifts',
            headerLeft: openDrawer(navigation),
            headerStyle: styles.bgColor,
            headerTintColor: styles.tintColor,
            headerTitleStyle: styles.titleStyle,
        })
    },
    ReportLocation: {
        screen: ReportLocation,
        navigationOptions: () => ({
            title: 'Shift location',
            headerStyle: styles.bgColor,
            headerTintColor: styles.tintColor,
            headerTitleStyle: styles.titleStyle,
        })
    }
});

export const AlertsStack = createStackNavigator({
    Alerts: {
        screen: AlertsPage,
        navigationOptions: ({ navigation }) => ({
            title: 'Alerts',
            headerLeft: openDrawer(navigation),
            headerStyle: styles.bgColor,
            headerTintColor: styles.tintColor,
            headerTitleStyle: styles.titleStyle,
        })
    }
});