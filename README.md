# WTS

WTS enables businesses to stay informed on the whereabouts of their assets.

## Installation

1. Use the package manager [npm](https://www.npmjs.com/) to install WTS packages.

```bash
npm install
```

*Try NOT to use **react-native-link** for linking node modules to avoid messy and incomplete bindings. 

2. Make sure to install **pod dependencies** for successful builds and archives on iOS. (Install CocoaPods before doing this)

```bash
cd ios
pod install
```

## Starting up

### Android

1. Open AVDs or connect android devices to run WTS.

```bash
cd <path_of_android_emulator>
emulator -avd <AVD_name>
```
2. Run it

```bash
react-native run-android
```
### iOS

```bash
react-native run-ios
```

## Debugging

- To debug on Android, `cmd + M` then press `Debug JS remotely`
- To debug on iOS, `cmd + D` then press `Debug JS remotely`

## Release APK [Android]

1. Update version code & version name on `android/app/build.gradle`
2. Make sure keystore is inside `android/app` folder
3. Make sure `android/gradle.properties` file matches keystore credentials

```bash
cd android
./gradlew assembleRelease
```

This release APK file shall be uploaded into Google Play Console by:
1. Choose WTS from Applications
2. `Release management > App Releases > Create Release`

## Archive/upload archive file [iOS]

1. Update version and build number on `XCode > app > Targets > app`
2. Clean build folder using `cmd + shift + K`
3. Build using `cmd + B`
4. Once build is successful, from the Top Tabs, navigate to `Product > Archive`
5. If archive is successful, select app and press `Distribute`
6. Select `Development` (if for testing purposes), or `Upload on App Store Connect`

### **In case of archive errors

e.g. *"Multiple commands produce...etc."*

Usually due to Pods and manual linking conflict. Possible steps to fix: 
1. Close XCode, then navigate to `~ > Library > Developer > XCode`
2. Delete DerivedData folder
3. In terminal,
```bash
pod deintegrate
pod clean
pod install
```
4. Open XCode, clean build folder and build. If build successful, try archiving again.


## Ownership
[SKYFY TECHNOLOGIES](https://skyfy.com.sg/)

Developed by: Ezra Yeoshua