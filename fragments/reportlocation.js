import React, { Component } from 'react';
import { Marker, PROVIDER_GOOGLE, Callout } from 'react-native-maps';
import { ClusterMap } from 'react-native-cluster-map';
import { View, Text } from 'react-native';
import { Icon } from 'react-native-elements';

//styles
import { styles } from '../assets/styles/reportlocationstyle';

const INITIAL_REGION = {
    latitude: 1.351616,
    longitude: 103.808053,
    latitudeDelta: 0.4000,
    longitudeDelta: 0.4000,
}

const EDGE_PADDING = {
    top: 150,
    right: 50,
    bottom: 150,
    left: 50
}

var moment = require('moment');
moment().format();
const dateformat = 'DD/MM/YYYY hh:mm:ss A';

export default class ReportLocation extends Component {
    constructor(props) {
        super(props);
        this.iconType = 'material-community';
        this.state = {
            draw: false,
            trackPoints: []
        }
    }

    generateDarkColor() {
        var lum = -0.25;
        var hex = String('#' + Math.random().toString(16).slice(2, 8).toUpperCase()).replace(/[^0-9a-f]/gi, '');
        if (hex.length < 6) {
            hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
        }
        var rgb = '#',
            c, i;
        for (i = 0; i < 3; i++) {
            c = parseInt(hex.substr(i * 2, 2), 16);
            c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
            rgb += ('00' + c).substr(c.length);
        }

        return rgb;
    }

    showMarkers = (params) => {
        let markers = [];
        let arr = params.latlngArray;
        let oddC = [];
        let evenC = [];
        let i = 0;

        for (i = 1; i < arr.length - 1; i += 2) {
            let color = this.generateDarkColor();
            oddC.push({ num: i, color })
            evenC.push({ num: i + 1, color })
        }

        let colors = [...oddC, ...evenC];

        colors.push({
            num: 0,
            color: '#000000'
        }, {
            num: arr.length,
            color: '#000000'
        })

        colors.sort((a, b) => a.num - b.num);

        arr.map((element, index) => {
            markers.push(
                <Marker
                    zIndex={i++}
                    style={styles.markerContainer}
                    key={'marker' + index}
                    ref={ref => this.marker = ref}
                    coordinate={{
                        latitude: element.latitude,
                        longitude: element.longitude
                    }}
                    title={element.location}
                    description={moment(new Date(parseInt(element.timestamp.substring(6, 19)))).format(dateformat)}>
                    <View style={[{ borderColor: colors[index].color + '50' }, styles.outerBorder]}>
                        <View style={[{ backgroundColor: colors[index].color }, styles.innerBorder]}>
                            {
                                (index === 0 || index === arr.length - 1) ? (
                                    <Icon name={index === 0 ? 'play' : 'stop'} size={15} iconStyle={{ color: '#FFFFFF' }} type='material-community' />
                                ) : <Text style={styles.innerText} > {index.toString()} </Text>

                            }
                        </View>
                    </View>
                </Marker>
            )
        });
        return markers;
    }

    renderClusterMarker = (count) => (
        <View style={styles.outerClusterMarker}>
            <View style={styles.clusterMarker}>
                <Text style={styles.clusterMarkerText}>{count}</Text>
            </View>
        </View>
    )

    render() {
        const { params } = this.props.navigation.state;

        if (!!params) {
            if (params.reportType === 'Shift') {
                return (
                    <View style={styles.container}>
                        <ClusterMap
                            renderClusterMarker={this.renderClusterMarker}
                            style={styles.map}
                            provider={PROVIDER_GOOGLE}
                            onMapReady={() => this.mapRef.mapRef.fitToCoordinates(params.latlngArray, { edgePadding: EDGE_PADDING })}
                            ref={(ref) => this.mapRef = ref}
                            region={INITIAL_REGION}>
                            {this.showMarkers(params)}
                        </ClusterMap>
                    </View>
                )
            }
        }
    }
}