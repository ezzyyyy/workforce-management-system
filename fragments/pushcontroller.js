import React, { Component } from 'react'
import { AsyncStorage, Platform } from 'react-native'
import Firebase from 'react-native-firebase'
import type, { Notification, NotificationOpen } from 'react-native-firebase'

import { Login } from '../api/authenticationapi'
import { GetUserCred } from '../api/asyncstorage'

export default class PushController extends Component {
    componentDidMount() {
        Firebase.messaging().hasPermission().then(hasPermission => {
            if (hasPermission) {
                this.getFCMToken();
                this.subscribeToNotificationListeners();
            } else {
                Firebase.messaging().requestPermission().then(() => {
                    this.getFCMToken();
                    this.subscribeToNotificationListeners();
                }).catch(error => {
                    // console.log(error)
                    alert('You have not allowed push notifications for Skyfy WTS. To receive push notifications, please enable it on the Settings app.');
                    return;
                });
            }
        })

        this.onTokenRefreshListener();
    }

    componentWillUnmount() {
        this.onTokenRefreshListener();
    }

    async getFCMToken() {
        let fcmToken = await AsyncStorage.getItem('FCM_TOKEN');
        console.log(fcmToken);
        if (!fcmToken) {
            fcmToken = await Firebase.messaging().getToken();
            if (fcmToken) {
                console.log(fcmToken);
                await AsyncStorage.setItem('FCM_TOKEN', fcmToken);
                let user = await GetUserCred();
                if (user) {
                    user = JSON.parse(user);
                    let u_name = user.u_name;
                    let u_pass = user.u_pass;
                    let res = await Login(u_name, u_pass, fcmToken);
                    let key = res.LogInResult;
                    if (key) {
                        console.log('Re-saved FCM token. [' + key + ']');
                        AsyncStorage.setItem('USER_TOKEN', key);
                        let UserCred = { u_name: u_name, u_pass: u_pass, u_fcmToken: fcmToken };
                        AsyncStorage.setItem('USER_CRED', JSON.stringify(UserCred));
                    }
                }
            }
        }
    }

    subscribeToNotificationListeners() {
        const channel = new Firebase.notifications.Android.Channel(
            'WTS-channel',
            'WTS Channel',
            Firebase.notifications.Android.Importance.Max)
            .setDescription('Channel for WTS Notifications');

        Firebase.notifications().android.createChannel(channel);

        this.notificationListener = Firebase.notifications().onNotification(async (notification) => {
            console.log('onNotification notification-->', notification);
            this.displayNotification(notification)
        });

        this.notificationOpenedListener = Firebase.notifications().onNotificationOpened((notification) => {
            this.props.goToAlerts();
        });

        // you can check the initial notification here
        this.getInitialNotificationListener = Firebase.notifications().getInitialNotification().then((notification) => {
            if (notification) {
                this.displayNotification(notification)
            }
        });
    }

    displayNotification = (notification) => {
        if (Platform.OS === 'android') {
            const localNotification = new Firebase.notifications.Notification({
                data: notification.data,
                sound: 'default',
                show_in_foreground: true,
                title: notification.title,
                body: notification.body
            }).setNotificationId(notification.notificationId)
                .setTitle(notification.title)
                .setSubtitle(notification.subtitle)
                .setBody(notification.body)
                .setData(notification.data)
                .android.setChannelId('WTS-channel') // e.g. the id you chose above
                .android.setSmallIcon('@drawable/notification_icon') // create this icon in Android Studio
                .android.setColor('#7dc6b7') // you can set a color here
                .android.setCategory(Firebase.notifications.Android.Category.Alarm)
                .android.setPriority(Firebase.notifications.Android.Priority.Max)
                .android.setVisibility(Firebase.notifications.Android.Visibility.Public)
                .android.setVibrate(1000)

            Firebase.notifications()
                .displayNotification(localNotification)
                .catch(err => console.error(err));

        } else if (Platform.OS === 'ios') {
            const localNotification = new Firebase.notifications.Notification()
                .setNotificationId(notification.notificationId)
                .setTitle(notification.title)
                .setSubtitle(notification.subtitle)
                .setBody(notification.body)
                .setData(notification.data)
                .ios.setBadge(notification.ios.badge)

            Firebase.notifications()
                .displayNotification(localNotification)
                .catch(err => console.error(err));
        }
    }

    async onTokenRefreshListener() {
        Firebase.messaging().onTokenRefresh(fcmToken => AsyncStorage.setItem('FCM_TOKEN', fcmToken));
    }

    render() {
        return null;
    }
}