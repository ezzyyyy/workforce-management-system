import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Platform } from 'react-native';
import { Icon } from 'react-native-elements';
import DatePicker from 'react-native-datepicker';
import Picker from 'react-native-picker-select';

//styles
import { reportHeaderStyles, pickerStyles, datePickerStyles } from '../assets/styles/reportheaderstyle'

//redux 
import { connect } from 'react-redux'
import { UpdateVehicle } from '../redux/actions/pages';

//asyncstorage
import { GetLastReportDetails } from '../api/asyncstorage';

var moment = require('moment');
moment().format();

class ReportHeader extends Component {
    _isMounted = false;
    constructor(props) {
        super(props);
        this.iconType = 'material-community';
        this.state = {
            token: undefined,
            user: undefined,
            startDate: moment().format('M/D/YY') + ' 00:00 AM',
            endDate: moment().format('M/D/YY h:mm A')
        };
    }

    pickerValChange(value) { this.setState({ user: value }) }

    startDatePickerValChange = (date) => { this.setState({ startDate: date }) }

    endDatePickerValChange = (date) => { this.setState({ endDate: date }) }

    async componentDidMount() {
        this._isMounted = true;
        this.props.UpdateVehicle();
        let lastReport = await GetLastReportDetails();
        if (!!lastReport) {
            let start = lastReport.start;
            let end = lastReport.end;
            start = lastReport.start.replace(/[^\d.]/g, '');
            end = lastReport.end.replace(/[^\d.]/g, '');
            if (this._isMounted) {
                this.setState({
                    token: lastReport.token,
                    user: lastReport.user,
                    startDate: moment.unix(start / 1000).format('M/D/YY h:mm A'),
                    endDate: moment.unix(end / 1000).format('M/D/YY h:mm A')
                })
            }
        }
    }

    componentWillUnmount() { this._isMounted = false; }

    getDetails() {
        let reportObject = {
            token: this.props.token,
            user: this.state.user,
            start: '/Date(' + Date.parse(new Date(this.state.startDate)) + ')/',
            end: '/Date(' + Date.parse(new Date(this.state.endDate)) + ')/'
        }
        this.props.passReportDetails(reportObject);
    }

    listVehicles = () => {
        let list = [];
        this.props.vehicles.vehicles.map((vehicle, index) => {
            if (vehicle.Category == 'Mobile Device') {
                list.push({ label: vehicle.Driver, value: vehicle.DriverID, key: index })
            }
        });
        return list;
    }

    render() {
        const { viewReport, hideHeader, viewCheckpoints } = this.props;
        const allFilledUp = (!!this.state.user && !!this.state.startDate && !!this.state.endDate);

        return (
            <View style={reportHeaderStyles.headerBody}>
                <View style={reportHeaderStyles.pickerGroupBody}>
                    <View style={reportHeaderStyles.pickerContainer}>
                        <Text style={reportHeaderStyles.label}>Start</Text>
                        <DatePicker
                            mode='datetime'
                            format='M/D/YY h:mm A'
                            showIcon={false}
                            date={this.state.startDate}
                            maxDate={new Date(this.state.endDate)}
                            confirmBtnText='Confirm'
                            cancelBtnText='Cancel'
                            customStyles={{ ...datePickerStyles }}
                            style={reportHeaderStyles.datePickerStyle}
                            onDateChange={this.startDatePickerValChange}
                        />
                    </View>
                    <View style={reportHeaderStyles.pickerContainer}>
                        <Text style={reportHeaderStyles.label}>End</Text>
                        <DatePicker
                            mode='datetime'
                            format='M/D/YY h:mm A'
                            showIcon={false}
                            date={this.state.endDate}
                            minDate={this.state.startDate}
                            maxDate={new Date(moment())}
                            confirmBtnText='Confirm'
                            cancelBtnText='Cancel'
                            customStyles={{ ...datePickerStyles }}
                            style={reportHeaderStyles.datePickerStyle}
                            onDateChange={this.endDatePickerValChange}
                        />
                    </View>
                </View>
                <View style={reportHeaderStyles.vehiclePickerBody}>
                    <Text style={reportHeaderStyles.label}>User</Text>
                    <Picker
                        items={this.listVehicles()}
                        placeholder={{ label: 'Select User' }}
                        placeholderTextColor='#555'
                        style={{ ...pickerStyles }}
                        value={this.state.user}
                        onValueChange={this.pickerValChange.bind(this)} />
                    <View style={reportHeaderStyles.searchBtnContainer}>
                        <TouchableOpacity
                            onPress={allFilledUp ?
                                () => {
                                    viewReport(this.state);
                                    viewCheckpoints(this.state.startDate, this.state.endDate, this.state.user);
                                    this.getDetails();
                                    hideHeader();
                                } : () => { }}
                            style={allFilledUp ? reportHeaderStyles.searchBtn : reportHeaderStyles.disabled}>
                            <Icon iconStyle={reportHeaderStyles.viewReportIcon} type={this.iconType} name='magnify' />
                            <Text style={reportHeaderStyles.searchText}>SEARCH</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        vehicles: state.vehicles,
        token: state.token
    }
}

const mapDispatchToProps = {
    UpdateVehicle,
};

export default connect(mapStateToProps, mapDispatchToProps)(ReportHeader)