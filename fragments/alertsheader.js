import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import DatePicker from 'react-native-datepicker';
import Picker from 'react-native-picker-select';

//styles
import { reportHeaderStyles, datePickerStyles } from '../assets/styles/reportheaderstyle'

//redux 
import { connect } from 'react-redux'
import { UpdateVehicle } from '../redux/actions/pages';

//asyncstorage
import { GetLastReportDetails } from '../api/asyncstorage';

var moment = require('moment');
moment().format();

class AlertsHeader extends Component {
    _isMounted = false;
    constructor(props) {
        super(props);
        this.iconType = 'material-community';
        this.state = {
            token: undefined,
            vehicle: undefined,
            startDate: moment().format('M/D/YY') + ' 00:00 AM',
            endDate: moment().format('M/D/YY h:mm A'),
            vehicles: [],
        };
    }

    pickerValChange(value) { this.setState({ vehicle: value }) }
    startDatePickerValChange = (date) => { this.setState({ startDate: date }) }
    endDatePickerValChange = (date) => { this.setState({ endDate: date }) }

    async componentDidMount() {
        this._isMounted = true;
        this.props.UpdateVehicle();
        let lastReport = await GetLastReportDetails();
        if (lastReport) {
            console.log(lastReport)
            let start = lastReport.start;
            let end = lastReport.end;
            start = lastReport.start.replace(/[^\d.]/g, '');
            end = lastReport.end.replace(/[^\d.]/g, '');
            if (this._isMounted) {
                this.setState({
                    token: lastReport.token,
                    vehicle: lastReport.vehicle,
                    startDate: moment.unix(start / 1000).format('M/D/YY h:mm A'),
                    endDate: moment.unix(end / 1000).format('M/D/YY h:mm A')
                })
            }
        }
    }

    componentWillUnmount() { this._isMounted = false; }

    getDetails() {
        let reportObject = {
            token: this.props.token,
            vehicle: 0,
            start: '/Date(' + Date.parse(new Date(this.state.startDate)) + ')/',
            end: '/Date(' + Date.parse(new Date(this.state.endDate)) + ')/'
        }
        this.props.passReportDetails(reportObject);
    }

    render() {
        const { viewReport, hideHeader } = this.props;

        return (
            <View style={reportHeaderStyles.headerBody}>
                <View style={[reportHeaderStyles.pickerGroupBody, { borderColor: 'transparent' }]}>
                    <View style={reportHeaderStyles.pickerContainer}>
                        <Text style={reportHeaderStyles.label}>Start</Text>
                        <DatePicker
                            mode='datetime'
                            format='M/D/YY h:mm A'
                            showIcon={false}
                            date={this.state.startDate}
                            maxDate={new Date(this.state.endDate)}
                            confirmBtnText='Confirm'
                            cancelBtnText='Cancel'
                            customStyles={{ ...datePickerStyles }}
                            style={reportHeaderStyles.datePickerStyle}
                            onDateChange={this.startDatePickerValChange}
                        />
                    </View>
                    <View style={reportHeaderStyles.pickerContainer}>
                        <Text style={reportHeaderStyles.label}>End</Text>
                        <DatePicker
                            mode='datetime'
                            format='M/D/YY h:mm A'
                            showIcon={false}
                            date={this.state.endDate}
                            minDate={this.state.startDate}
                            maxDate={new Date(moment())}
                            confirmBtnText='Confirm'
                            cancelBtnText='Cancel'
                            customStyles={{ ...datePickerStyles }}
                            style={reportHeaderStyles.datePickerStyle}
                            onDateChange={this.endDatePickerValChange}
                        />
                    </View>
                </View>
                <View style={reportHeaderStyles.vehiclePickerBody}>
                    <View style={reportHeaderStyles.searchBtnContainer}>
                        <TouchableOpacity
                            onPress={
                                () => {
                                    viewReport();
                                    this.getDetails();
                                    hideHeader();
                                }}
                            style={reportHeaderStyles.searchBtn}>
                            <Icon iconStyle={reportHeaderStyles.viewReportIcon} type={this.iconType} name='magnify' />
                            <Text style={reportHeaderStyles.searchText}>SEARCH</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        vehicles: state.vehicles,
        token: state.token
    }
}

const mapDispatchToProps = {
    UpdateVehicle,
};

export default connect(mapStateToProps, mapDispatchToProps)(AlertsHeader) 