import React, { Component } from 'react';
import { Text, FlatList, View, TouchableOpacity, ActivityIndicator } from 'react-native';
import { Icon, Card } from 'react-native-elements';
import Hyperlink from 'react-native-hyperlink'

//redux 
import { connect } from 'react-redux'

//API
import { GetPositionReport } from '../api/reportapi'

//asyncstorage
import { GetCompanyID } from '../api/asyncstorage'

var moment = require('moment');
moment().format();

//styles
import { styles, speedingStyles, messageStyles } from '../assets/styles/resultliststyle'

class ResultList extends Component {
    constructor(props) {
        super(props);
        this.iconType = 'material-community';
        this.state = {
            vehicle: undefined,
            driver: undefined,
            vehicles: [],
            trackPoints: []
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (prevState !== nextProps) {
            return {
                dataArray: nextProps.dataArray
            }
        }
    }

    showTracks = async (startDate, endDate, vehicleId) => {
        this.setState({ trackPoints: [], showLoader: true });
        let positions = [];
        let count = 300;
        let page = 0;
        let token = this.props.token;

        let company = await GetCompanyID();

        try {
            while (count === 300) {
                let result = await GetPositionReport(token, startDate, endDate, vehicleId, page);
                result = result.GetPositionReportResult;
                count = result.length;

                if (count === 0) {
                    alert('No trackpoints recorded for this shift.');
                } else {
                    Array.prototype.push.apply(positions, result);
                    if (count > 0)
                        posID = result[count - 1].PosID;
                }
            }
            if (count !== 0) {
                positions.sort(function (a, b) { return a.PosID - b.PosID })
                this.setState({ trackPoints: positions }, () => this.showMap());
            }
        } catch (error) {
            alert(error)
        }
    }

    showMap = () => {
        setTimeout(() => {
            this.props.goToShiftMap({
                reportType: 'Shift',
                latlngArray: this.state.trackPoints.map(tp => {
                    let loc = tp.Location;
                    let cut = loc.split(',');
                    cutLoc = (cut[0] + ',' + cut[1] + ',' + cut[2]);

                    return {
                        latitude: tp.Latitude,
                        longitude: tp.Longitude,
                        location: cutLoc,
                        timestamp: tp.Timestamp
                    }
                })
            })
            this.setState({ showLoader: false })
        }, 2000);
    }

    _renderShiftRow = ({ item }) => {
        let cPoints = [];
        let sTime = '00:00:00 AM';
        let sDate = 'M/DD/YYYY';
        let eTime = '00:00:00 AM';
        let eDate = 'M/DD/YYYY';
        let dur = '-';
        let cutEndLoc = 'No end location recorded. Shift may be ongoing...';
        let cutStartLoc = 'No start location recorded';

        if (!!item.EndPosition && !!item.StartPosition) {
            let startDate = parseInt(item.ActualStart.substring(6, 19));

            sDate = moment(startDate).format('l');
            sTime = moment(startDate).format('LTS');

            let locStart = item.StartPosition.Location;
            let cutStart = locStart.split(',');
            cutStartLoc = (cutStart[0] + ',' + cutStart[1]);

            let endDate = parseInt(item.ActualEnd.substring(6, 19));

            eTime = moment(endDate).format('LTS');
            eDate = moment(endDate).format('l');

            let locEnd = item.EndPosition.Location;
            let cutEnd = locEnd.split(',');
            cutEndLoc = (cutEnd[0] + ',' + cutEnd[1]);

            dur = moment.duration({ 'minutes': item.ActualDuration }).format('d.HH:mm:ss');

            this.props.checkPointsArray.map((p, index) => {
                let ts = parseInt(p.Timestamp.substring(6, 19));
                if (ts >= startDate && ts <= endDate) {
                    let loc = p.Location;
                    let cut = loc.split(',');
                    pLoc = (cut[0] + ',' + cut[1]);
                    cPoints.push(pLoc)
                }
                return cPoints
            })
        }
        cPoints.shift()
        console.log(cPoints)

        return (
            <Card>
                <TouchableOpacity
                    style={styles.cardItem}
                    onPress={() => {
                        if (!!item.EndPosition && !!item.StartPosition) {
                            this.showTracks(
                                item.StartPosition.Timestamp,
                                item.ActualEnd,
                                item.VehicleID,
                                item.StartPosition.Location,
                                item.EndPosition.Location)
                        } else {
                            //do nothing
                        }
                    }}>
                    {!!this.state.showLoader &&
                        <ActivityIndicator
                            style={{
                                backgroundColor: '#ffffff20',
                                position: 'absolute',
                                top: 0,
                                bottom: 0,
                                right: 0,
                                left: 0
                            }}
                            size='small'
                            color='#99999950' />}
                    <View style={speedingStyles.detailsBody}>
                        <View style={speedingStyles.locationBody}>
                            <Text style={speedingStyles.label}>Start: </Text>
                            <Text style={speedingStyles.locMarquee}>{cutStartLoc}</Text>
                        </View>
                        <View style={speedingStyles.timeBody}>
                            <View style={speedingStyles.startTimeGroup}>
                                <Text style={speedingStyles.time}>{sTime}</Text>
                                <Text style={speedingStyles.date}>{sDate}</Text>
                            </View>
                            <View style={speedingStyles.lineDurationBody}>
                                <View style={speedingStyles.line} />
                                <Text style={speedingStyles.duration}>{dur}</Text>
                            </View>
                            <View style={speedingStyles.endTimeGroup}>
                                <Text style={speedingStyles.time}>{eTime}</Text>
                                <Text style={speedingStyles.date}>{eDate}</Text>
                            </View>
                        </View>
                        <View style={{ flex: 1 }}>
                            {
                                cPoints.map((p, index) => (
                                    <View key={index} style={{ flexDirection: 'row' }}>
                                        <Text style={{ fontSize: 12, color: '#555' }}>{(index % 2 === 0) ? 'CHECK-IN: ' : 'CHECK-OUT: '}</Text>
                                        <Text style={{ fontSize: 12, color: '#555' }}>{p}</Text>
                                    </View>
                                ))
                            }
                        </View>
                        <View style={speedingStyles.locationBody}>
                            <Text style={speedingStyles.label}>End: </Text>
                            <Text style={speedingStyles.locMarquee}>{cutEndLoc}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </Card>
        )
    }

    _renderMessageRow = ({ item }) => {
        const date = (parseInt(item.Timestamp.substring(6, 19)));
        return (
            <View
                style={messageStyles.sectionContainer}
                key={item.MessageID}>
                <View style={messageStyles.detailsContainer}>
                    <Hyperlink linkStyle={messageStyles.linkStyle} linkDefault={true}>
                        <Text style={messageStyles.body}>{item.Message}</Text>
                        <Text style={messageStyles.time}>
                            {moment(new Date(date)).format('l') + ' ' + moment(new Date(date)).format('LTS')}
                        </Text>
                    </Hyperlink>
                </View>
            </View>
        )
    }

    render() {
        const { dataArray, resultType } = this.props;
        let res;
        let _keyExtractor;
        if (resultType === 'message') {
            res = this._renderMessageRow;
            _keyExtractor = (item, index) => item.MessageID.toString();
            dataArray.sort(function (a, b) { return b.MessageID - a.MessageID });
        } else if (resultType === 'shift') {
            res = this._renderShiftRow;
            _keyExtractor = (item, index) => item.ShiftID.toString();
            dataArray.sort(function (a, b) { return b.ShiftID - a.ShiftID });
        }

        return (
            <FlatList
                contentContainerStyle={{ paddingBottom: 10 }}
                style={styles.list}
                keyExtractor={_keyExtractor}
                data={dataArray}
                renderItem={res}
            />
        )
    }

}

function mapStateToProps(state) {
    return {
        token: state.token
    }
}

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(ResultList)