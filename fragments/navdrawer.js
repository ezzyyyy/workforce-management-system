import React from 'react';
import { View, Text, Image, TouchableOpacity, Alert, AsyncStorage, SafeAreaView } from 'react-native';
import { Icon } from 'react-native-elements';
import Intercom from 'react-native-intercom';

//style
import { styles } from '../assets/styles/navdrawerstyle'

//redux 
import { connect } from 'react-redux'
import { EmptyProps, UpdateVehicle, ToggleUpdateVehicle } from '../redux/actions/pages';

//asyncstorage
import { GetToken, GetCompanyID } from '../api/asyncstorage';

//api
import { Logout } from '../api/authenticationapi';

class MainDrawerNavigation extends React.Component {
    constructor(props) {
        super(props);
        this.bg = { backgroundColor: '#f2f2f2' };
        this.iconType = 'material-community';
        this.state = {
            companies: [],
            selected: undefined
        }
    }

    navigateToScreen = (props, route) => {
        //enable/disable vehicle api
        if (route === 'LiveView' || route === 'People')
            props.ToggleUpdateVehicle(true);
        else 
            props.ToggleUpdateVehicle(false);

        props.navigation.navigate(route);
    }

    componentDidMount() {
        GetCompanyID().then((id) => this.setState({ selected: id }))
    }

    renderUserData = () => {
        if (this.props.userData) {
            let photo = this.props.userData.user.Photo;
            return (
                <View style={styles.userContainer}>
                    {!!photo && <Image source={{ uri: photo }} style={styles.image} />}
                    <View style={styles.detailsContainer}>
                        <Text style={styles.userName}>{this.props.userData.user.Name}</Text>
                        <Text style={styles.userEmail}>{this.props.userData.user.Email}</Text>
                    </View>
                </View>
            )
        }
    }

    onValueChange(value) {
        AsyncStorage.setItem('COMPANY_ID', JSON.stringify(value));
        this.setState({ selected: value });
    }

    listCompany = () => {
        let comp = [];
        if (this.props.userData && this.props.userData.companies) {
            this.props.userData.companies.map((element, index) => {
                comp.push({ label: element.company, value: index, key: index });
            });
        }
        return comp;
    }

    logout = async () => {
        let shiftStatus = await AsyncStorage.getItem('SHIFT');
        shiftStatus = JSON.parse(shiftStatus);
        if (shiftStatus !== null) {
            Alert.alert(
                'Shift in progress...',
                'You are currently attending to a shift. Please stop your shift before logging out.'
            );
        } else {
            Alert.alert(
                'Logout',
                'Are you sure?',
                [
                    { text: 'Cancel', onPress: () => { } },
                    {
                        text: 'Yes', onPress: async () => {
                            let token = await GetToken();
                            if (token) {
                                setTimeout(() => {
                                    this.props.navigation.navigate('Auth');
                                    Intercom.logout();
                                    AsyncStorage.clear();
                                    this.props.EmptyProps();
                                }, 1000);

                                Logout(token).then(res => {
                                    if (!!res) {
                                        if (res.LogOutResult.Message == 'OK') {
                                            console.log('offline');
                                        } else {
                                            console.log(res.LogOutResult.Message);
                                        }
                                    } else {
                                        console.log(res)
                                    }
                                })
                            }
                        }
                    }
                ])
        }
    }

    openIntercom = () => Intercom.displayMessageComposer()

    render() {
        const pages = [
            {
                key: 'LiveView',
                name: 'Live View',
                icon: 'map',
                onPress: () => this.navigateToScreen(this.props, 'LiveView')
            },
            {
                key: 'People',
                name: 'People',
                icon: 'account-group',
                onPress: () => this.navigateToScreen(this.props, 'People'),
            },
            {
                key: 'ShiftReport',
                name: 'Shifts',
                icon: 'timer-sand',
                onPress: () => this.navigateToScreen(this.props, 'ShiftReport'),
            },
            {
                key: 'Alerts',
                name: 'Alerts',
                icon: 'alert',
                onPress: () => this.navigateToScreen(this.props, 'Alerts'),
            },
            {
                key: 'Support',
                name: 'Support',
                icon: 'forum',
                onPress: this.openIntercom
            },
        ]

        return (
            <SafeAreaView style={styles.container}>
                {this.renderUserData()}
                <View style={styles.navContainer}>
                    {
                        pages.map((page, index) => {
                            return (
                                <TouchableOpacity
                                    key={index}
                                    onPress={page.onPress}
                                    style={styles.navItemContainer}>
                                    <Icon
                                        type='material-community'
                                        name={page.icon}
                                        iconStyle={styles.navIcon}
                                    />
                                    <Text
                                        style={[styles.pageName, {
                                            fontWeight: this.props.activeItemKey == page.key ? 'bold' : 'normal',
                                        }]}>{page.name}</Text>
                                </TouchableOpacity>
                            )
                        })
                    }
                    <View style={styles.logoutContainer}>
                        <TouchableOpacity
                            onPress={this.logout}
                            style={styles.logoutBtn}>
                            <Icon
                                type='material-community'
                                name='logout'
                                iconStyle={styles.logoutIcon}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

function mapStateToProps(state) {
    return {
        vehicles: state.vehicles,
        userData: state.userData
    }
}

const mapDispatchToProps = {
    EmptyProps,
    UpdateVehicle,
    ToggleUpdateVehicle
};

export default connect(mapStateToProps, mapDispatchToProps)(MainDrawerNavigation)