import React from 'react';
import { View } from 'react-native';
import { Icon } from 'react-native-elements';
import MapView, { Polyline, Marker, PROVIDER_GOOGLE } from 'react-native-maps'

import LiveViewStyle from '../assets/styles/liveviewstyle'
import { MarkerStyle } from '../assets/styles/markerstyle'

const INITIAL_REGION = {
    latitude: 1.290270,
    longitude: 103.851959,
    latitudeDelta: 0.3000,
    longitudeDelta: 0.3000
}

class MapTracks extends React.Component {
    constructor(props) {
        super(props);
        this.lineColor = 'rgba(230, 126, 34,1.0)';
        this.iconType = 'material-community'
    }

    showMarkers = () => {
        let markers = [];
        let markersLatLng = [];

        markers = [
            {
                latlng: {
                    latitude: this.props.latLng[this.props.latLng.length - 1].latitude,
                    longitude: this.props.latLng[this.props.latLng.length - 1].longitude,
                },
                title: 'Start',
                color: '#e67e22',
                icon: 'near-me',
                downIcon: 'menu-down'
            },
            {
                latlng: {
                    latitude: this.props.latLng[0].latitude,
                    longitude: this.props.latLng[0].longitude,
                },
                title: 'End',
                color: '#2ecc71',
                icon: 'flag-checkered',
                downIcon: 'menu-down'
            }
        ];

        markers.map((element, index) => {
            markersLatLng.push(
                <Marker
                key={index}
                style={MarkerStyle.markerContainer}
                title={element.title}
                coordinate={element.latlng}>
                <View style={[MarkerStyle.vehIcon, { borderColor: element.color }]}>
                    <Icon
                        iconStyle={{ color: element.color }}
                        name={element.icon}
                        type={this.iconType}  />
                </View>
                <Icon
                    iconStyle={[{ color: element.color }, MarkerStyle.downIcon]}
                    name={element.downIcon}
                    type={this.iconType}  />
            </Marker>
            )
        });
        return markersLatLng;
    }

    mapReady = () => this.mapRef.fitToCoordinates(this.props.latLng);

    render() {
        return (
            <View style={MarkerStyle.container}>
                <MapView
                    style={LiveViewStyle.map}
                    provider={PROVIDER_GOOGLE}
                    loadingEnabled
                    onMapReady={this.mapReady}
                    ref={(ref) => this.mapRef = ref}
                    initialRegion={INITIAL_REGION}>
                    <Polyline
                        key='track-1'
                        coordinates={this.props.latLng}
                        strokeColor={this.lineColor}
                        fillColor={this.lineColor}
                        strokeWidth={3} />
                    {this.showMarkers()}
                </MapView>
            </View>
        )
    }
}

export default MapTracks;