import React from "react";
import { View } from "react-native";
import { Icon } from 'react-native-elements';
import { Marker, Polyline, Polygon, Circle } from 'react-native-maps'
//styles
import {GeoMarkerStyle} from '../assets/styles/markerstyle'
class Geofences extends React.Component {
    getDistance = (latLong) => {
        var R = 6371; // Radius of the earth in km
        var dLat = this.deg2rad(latLong[1].latitude-latLong[0].latitude);  // this.deg2rad below
        var dLon = this.deg2rad(latLong[1].longitude-latLong[0].longitude); 
        var a = 
          Math.sin(dLat/2) * Math.sin(dLat/2) +
          Math.cos(this.deg2rad(latLong[0].latitude)) * Math.cos(this.deg2rad(latLong[1].latitude)) * 
          Math.sin(dLon/2) * Math.sin(dLon/2);

        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
        var d = R * c; // Distance in km
        return d;
    }

    deg2rad = (deg) => {
        return deg * (Math.PI/180)
    }

    hexToRgbA = (hex, op) => {
        var c;
        if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
            c= hex.substring(1).split('');
            if(c.length== 3){
                c= [c[0], c[0], c[1], c[1], c[2], c[2]];
            }
            c= '0x'+c.join('');
            return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+',' + op +')';
        }
        throw new Error('Bad Hex');
    }

    listGeofences = () => {
        var geofences = [];
        if(this.props.geofences)
        {
            this.props.geofences.map(g => {
                var latLong = [];
                g.Path.map(p => {
                    latLong.push({latitude: p.Latitude, longitude: p.Longitude});
                });
                if(g.Shape == 3) {
                    geofences.push(<Marker style={ GeoMarkerStyle.container } identifier={"geo" + g.GeofenceID} key={"geo" + g.GeofenceID}
                    coordinate={{latitude: latLong[0].latitude, longitude: latLong[0].longitude}}
                    title={g.Name}>
                        <View style={ [GeoMarkerStyle.vehIcon, {borderColor: g.Color}] }></View>
                        <Icon iconStyle={{color: g.Color, padding:0, margin:0, marginTop: -14, fontSize: 26}} name="menu-down" type="material-community" />
                    </Marker>)
                } else if(g.Shape == 2) {
                    geofences.push(<Polyline
                        key={"geo" + g.GeofenceID}
                        coordinates={latLong}
                        strokeColor={this.hexToRgbA(g.Color, 0.5)}
                        strokeWidth={1}
                        title={g.Name}
                        lineDashPattern={[5, 2, 3, 2]} />)
                } else if(g.Shape == 1) {
                    geofences.push(<Polygon
                        key={"geo" + g.GeofenceID}
                        coordinates={latLong}
                        fillColor={this.hexToRgbA(g.Color, 0.5)}
                        strokeColor={g.Color}
                        strokeWidth={1} />);
                } else if(g.Shape == 0 || g.Shape == undefined) {
                    geofences.push(<Circle
                        key={"geo" + g.GeofenceID}
                        center={{latitude: latLong[0].latitude, longitude: latLong[0].longitude}}
                        radius={this.getDistance(latLong) * 1000}
                        fillColor={this.hexToRgbA(g.Color, 0.5)}
                        strokeColor={g.Color}
                        zIndex={3}
                        strokeWidth={1} />);
                }
            })
        }

        return geofences;
    }

    render() {
        return this.listGeofences()
    }
}

export default Geofences;