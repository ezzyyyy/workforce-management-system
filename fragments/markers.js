import React from 'react';
import { View, Text, Image } from 'react-native';
import { Marker, Callout } from 'react-native-maps'
import moment from 'moment';

//assets
import p_green from '../assets/imgs/icons/person_green.png';
import p_grey from '../assets/imgs/icons/person_grey.png';
//styles
import { styles } from '../assets/styles/markerstyle';

export default class LiveMapMarker extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            tracksViewChanges: true
        }
    }

    componentDidUpdate() {
        if (this.state.tracksViewChanges)
            this.setState({ tracksViewChanges: false });
    }

    render() {
        let v = this.props.vinfo;
        enStat = p_grey;
        if (v.LastFix) {
            if (moment().diff(moment(v.LastFix.Timestamp), 'days') == 0) {
                if (v.LastFix.Engine == 1)
                    enStat = p_grey;
                else if (v.LastFix.Engine == 2)
                    enStat = p_green;
            }
            else enStat = p_grey;
        }
        else enStat = p_grey;

        return (
            <Marker
                onPress={this.props.onPress}
                identifier={'marker' + v.VehicleID}
                coordinate={{ latitude: v.LastFix.Latitude, longitude: v.LastFix.Longitude }}
                tracksViewChanges={this.state.tracksViewChanges}>
                <View style={styles.markerView}>
                    <View style={styles.calloutView}>
                        <Text style={styles.calloutText}>{v.Name}</Text>
                    </View>
                    <View style={styles.imageView}>
                        <Image 
                            style={styles.image} 
                            source={enStat} 
                            onLoad={() => this.forceUpdate()} 
                        />
                    </View>
                </View>
                <Callout style={styles.transparentCallout}>
                </Callout>
            </Marker>
        );
    }
}