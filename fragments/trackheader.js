import React, { Component } from 'react';
import DatePicker from 'react-native-datepicker';
import { Text, View, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import Picker from 'react-native-picker-select';

//styles
import { reportHeaderStyles, pickerStyles, datePickerStyles } from '../assets/styles/reportheaderstyle'

//redux 
import { connect } from 'react-redux'
import { UpdateVehicle } from '../redux/actions/pages';

//asyncstorage
import { GetLastReportDetails } from '../api/asyncstorage';

var moment = require('moment');
moment().format();

class TrackHeader extends Component {
    _isMounted = false;
    constructor(props) {
        super(props);
        this.iconType = 'material-community';
        this.state = {
            vehicles: [],
            vehicle: undefined,
            startDate: moment().format('M/D/YY') + ' 00:00 AM',
            endDate: moment().format('M/D/YY h:mm A'),
        };
    }

    async componentDidMount() {
        this._isMounted = true;
        this.props.UpdateVehicle();
        let lastReport = await GetLastReportDetails();
        if (!!lastReport) {
            let start = lastReport.start;
            let end = lastReport.end;
            start = lastReport.start.replace(/[^\d.]/g, '');
            end = lastReport.end.replace(/[^\d.]/g, '');
            if (this._isMounted) {
                this.setState({
                    token: lastReport.token,
                    vehicle: lastReport.vehicle,
                    startDate: moment.unix(start / 1000).format('M/D/YY h:mm A'),
                    endDate: moment.unix(end / 1000).format('M/D/YY h:mm A')
                })
            }
        }
    }

    componentWillUnmount() { this._isMounted = false; }

    pickerValChange(value) { this.setState({ vehicle: value }) }

    startDatePickerValChange = (date) => { this.setState({ startDate: date }) }

    endDatePickerValChange = (date) => { this.setState({ endDate: date }) }

    listVehicles = () => {
        let list = [];
        this.props.vehicles.vehicles.map((v, index) => {
            if (v.Category == 'Mobile Device') {
                list.push({ label: v.Name, value: v.VehicleID, key: index });
            }
        });
        return list;
    }

    render() {
        const { hideHeader } = this.props;
        const allFilledUp = (!!this.state.vehicle && !!this.state.startDate && !!this.state.endDate);

        return (
            <View style={reportHeaderStyles.headerBody}>
                <View style={reportHeaderStyles.pickerGroupBody}>
                    <View style={reportHeaderStyles.pickerContainer}>
                        <Text style={reportHeaderStyles.label}>Start</Text>
                        <DatePicker
                            mode='datetime'
                            format='M/D/YY h:mm A'
                            showIcon={false}
                            date={this.state.startDate}
                            maxDate={new Date(this.state.endDate)}
                            confirmBtnText='Confirm'
                            cancelBtnText='Cancel'
                            style={reportHeaderStyles.date}
                            customStyles={{ ...datePickerStyles }}
                            onDateChange={this.startDatePickerValChange}
                        />
                    </View>
                    <View style={reportHeaderStyles.pickerContainer}>
                        <Text style={reportHeaderStyles.label}>End</Text>
                        <DatePicker
                            mode='datetime'
                            format='M/D/YY h:mm A'
                            showIcon={false}
                            date={this.state.endDate}
                            minDate={this.state.startDate}
                            maxDate={new Date(moment())}
                            confirmBtnText='Confirm'
                            cancelBtnText='Cancel'
                            style={reportHeaderStyles.datepickerStyle}
                            customStyles={{ ...datePickerStyles }}
                            onDateChange={this.endDatePickerValChange}
                        />
                    </View>
                </View>
                <View style={reportHeaderStyles.vehiclePickerBody}>
                    <Text style={reportHeaderStyles.label}>User</Text>
                    <Picker
                        items={this.listVehicles()}
                        placeholder={{ label: 'Select User' }}
                        placeholderTextColor='#555'
                        style={{ ...pickerStyles }}
                        value={this.state.vehicle}
                        onValueChange={this.pickerValChange.bind(this)} />
                    <View style={reportHeaderStyles.searchBtnContainer}>
                        <TouchableOpacity
                            onPress={allFilledUp ? () => {
                                this.props.getTracks(this.state);
                                hideHeader();
                            } : () => { }}
                            style={allFilledUp ? reportHeaderStyles.searchBtn : reportHeaderStyles.disabled}>
                            <Icon iconStyle={reportHeaderStyles.viewReportIcon} type={this.iconType} name='magnify' />
                            <Text style={reportHeaderStyles.searchText}>SEARCH</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        vehicles: state.vehicles,
        token: state.token
    }
}

const mapDispatchToProps = {
    UpdateVehicle,
};

export default connect(mapStateToProps, mapDispatchToProps)(TrackHeader)