import { AsyncStorage } from 'react-native';

export const GetToken = async () => {
    const token = await AsyncStorage.getItem('USER_TOKEN');
    return token;
}

export const GetUserData = async () => {
    const user = await AsyncStorage.getItem('USER');
    return user;
}

export const GetCompanies = async () => {
    const companies = await AsyncStorage.getItem('USER_COMPANIES');
    return companies;
}

export const GetCompanyID = async () => {
    const companyid = await AsyncStorage.getItem('COMPANY_ID');
    return JSON.parse(companyid);
}

export const GetLastReportDetails = async () => {
    const lastreportdetails = await AsyncStorage.getItem('LAST_REPORT_DETAILS');
    return JSON.parse(lastreportdetails);
}

export const GetAlerts = async () => {
    const alerts = await AsyncStorage.getItem('ALERTS');
    return JSON.parse(alerts);
}

export const GetFCMToken = async () => {
    const fcmToken = await AsyncStorage.getItem('FCM_TOKEN');
    return fcmToken;
}

export const GetUserCred = async () => {
    const userCred = await AsyncStorage.getItem('USER_CRED');
    return userCred;
}
