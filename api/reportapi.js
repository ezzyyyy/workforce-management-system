
import { ApiEndpoint, ApiGetPositionReport, ApiGetJourneyReport, ApiGetMessageReport, ApiGetShiftReport } from './api'

export const GetJourneyReport = async (token, start, end, user, page) => {
  return fetch(ApiEndpoint + ApiGetJourneyReport, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      Key: token,
      Start: start,
      End: end,
      Filter: {
        DriverID: user,
        JourneyID: page
      }
    }),
  })
    .then((response) => response.json())
}

export const GetPositionReport = async (token, start, end, vehicle, posID) => {
  return fetch(ApiEndpoint + ApiGetPositionReport, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      Key: token,
      Start: start,
      End: end,
      Filter: {
        VehicleID: vehicle,
        PosID: posID
      }
    })
  })
    .then((response) => response.json());
};

export const GetMessageReport = async (token, vehicle, start, end, page, company) => {
  return fetch(ApiEndpoint + ApiGetMessageReport, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      Key: token,
      Start: start,
      End: end,
      Filter: {
        MessageID: page,
        TypeID: -1,
        VehicleID: vehicle,
        Group: {
          CompanyID: company
        }
      },
    })
  })
    .then((response) => response.json());
};

export const GetShiftReport = async (token, start, end, user, page) => {
  return fetch(ApiEndpoint + ApiGetShiftReport, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      Key: token,
      Start: start,
      End: end,
      Filter: {
        DriverID: user,
        ShiftID: page
      }
    }),
  })
    .then((response) => response.json())
}