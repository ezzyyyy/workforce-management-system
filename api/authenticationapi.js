import { ApiEndpoint, ApiLogin, ApiGetUser, ApiGetCompanies, ApiResetPassword, ApiLogout } from './api'

export const Login = async (username, password, deviceToken) => {
  console.log('user: ' + username, 'pass: ' + password, 'FCM: ' + deviceToken)
  return fetch(ApiEndpoint + ApiLogin, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      Username: username,
      Password: password,
      Reserved: '4|' + deviceToken
    }),
  })
    .then((response) => response.json())
}

export const GetUser = async (token) => {
  return fetch(ApiEndpoint + ApiGetUser, {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
      },
      body: JSON.stringify({
          Key: token
      }),
  })
  .then((response) => {
    if (response.status === 200) {
      return response.json()
    } else {
      console.log('Failed to retrieve user data');
    }
  })
}

export const GetCompanies = async (token, page) => {
  return fetch(ApiEndpoint + ApiGetCompanies, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      Key: token,
      CompanyID: page,
    }),
  })
  .then((response) => {
    if (response.status === 200) {
      return response.json()
    } else {
      console.log(`Failed to retrieve companies' data`);
    }
  })
}

export const ResetPassword = async (username) => {
  return fetch(ApiEndpoint + ApiResetPassword, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      User: username
    }),
  })
  .then((response) => response.json())
}

export const Logout = async (token) => {
  return fetch(ApiEndpoint + ApiLogout, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      Key: token,
      Reset: "false"
    }),
  })
    .then((response) => {
      if (response.status === 200)
        return response.json()
      else console.log(response.status)
    })
}