import { ApiEndpoint, ApiStartShift, ApiEndShift, ApiGetEventReport, ApiGetUser } from './api'

export const StartShift = async (token, timestamp) => {
    return fetch(ApiEndpoint + ApiStartShift, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            Key: token,
            Timestamp: timestamp
        }),
    }).then((response) => response.json())
}

export const EndShift = async (token, timestamp) => {
    return fetch(ApiEndpoint + ApiEndShift, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            Key: token,
            Timestamp: timestamp
        }),
    }).then((response) => response.json())
}

export const GetEventReport = async (token, start, end,  user, page) => {
    return fetch(ApiEndpoint + ApiGetEventReport, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            Key: token,
            Start: start,
            End: end,
            Type: [46,47],
            Filter: {
                DriverID: user,
                EventID: page
            }
        }),
    }).then((response) => response.json())
}