import { skey, ApiEndpoint, ApiVehicles, ApiGeofences, ApiImmobilizer, ApiSavePosition } from './api'

export const GetVehicles = async (token) => {
  return fetch(ApiEndpoint + ApiVehicles, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      "Key": token,
      "Locate": true,
      "All": true
    })
  })
    .then((response) => {
      if (response.status === 200) {
        return response.json()
      } else {
        console.log('Failed to retrieve vehicle data');
      }
    })
}

export const ImmobilizeVehicle = async (token, TrackerID, Status) => {
  return fetch(ApiEndpoint + ApiImmobilizer, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      "Key": token,
      "TrackerIds": [TrackerID],
      "Command": { "ReservedID": 0, "Reserved": Status }
    })
  })
    .then((response) => response.json())
}

export const GetGeofences = async (token) => {
  // 0 = Circle
  // 1 = Polygon
  // 2 = Polyline
  // 3 = Point
  return fetch(ApiEndpoint + ApiGeofences, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      "Key": token,
      "Group": {
        "CompanyID": 1
      },
      "All": false
    })
  })
    .then((response) => {
      if (response.status === 200) {
        return response.json()
      } else {
        console.log('Failed to retrieve geofence data');
      }
    })
}

export const SaveLocation = async (token, position) => {
  return fetch(ApiEndpoint + ApiSavePosition, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      "Key": token,
      "Positions": position
    })
  })
}